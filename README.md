# 和平之翼代码生成器SHCEU版

## 本生成器已全面支持前后端分离界面(Excel,SGS)

欢迎大家使用由无垠式，和平之翼和光三代动词算子式代码生成器组成的动词算子式代码生成器阵列，在我的码云站点[https://gitee.com/jerryshensjf/](https://gitee.com/jerryshensjf/)大家可以找到这些代码生成器。把他们统统部署在Tomcat中，您可以获得超过600N的代码变形能力。

### 项目介绍
和平之翼代码生成器SHCEU版，SHCEU技术栈，JQuery Easy UI, Spring MVC 4.2, Spring 4.2, Hibernate Criteria。

现在，和平之翼代码生成器SHCEU 4.0.0 版 研发代号：千年隼 Millennium Falcon 已发布RC4候选版本。此版本支持一个源文件(Excel,SGS)生成一个SHCEU技术栈的后端项目和一个使用Vue + ElementUI的配套的前端项目。并增加了您在开发时很有用处的小工具。

此软件是动词算子式代码生成器阵列的最新成员。基于和平之翼代码生成器SMEU版 3.2 乌篷船的最新代码重新制作，并移植了和平之翼代码生成器4.0.0的先进特性，服务于无垠式/和平之翼代码生成器阵列的s2sh,s2shc和shc技术栈的原有用户，可以使用本代码生成器阵列的所有先进技术。

在本站附件处下载二进制war包。
[https://gitee.com/jerryshensjf/PeaceWingSHCEU/attach_files](https://gitee.com/jerryshensjf/PeaceWingSHCEU/attach_files)

### 重要更新
和平之翼SHCEU 4.0.0 RC5 是一个重要更新，修复了windows下不能代码生成的缺陷，RC5版的兼容性大大提升，请大家尽快更新到此版本 

已释出和平之翼SHCEU 4.0.0 RC5候选版。已修复了多重一对多和其他一些缺陷。整体功能达到和平之翼SMEU 4.1.0 Beta5水平。现在已完善对Oracle的支持。

### 同时生成前端和后端项目截图

![输入图片说明](https://images.gitee.com/uploads/images/2019/0630/225401_b8c85d70_1203742.png "MF_dualproject.png")

### 前端项目

本生成器已支持Vue+ElementUI前端项目生成，从Vue-element-admin派生，感谢原作者的作品。

### 前端项目截图：
登录：

![登录](https://images.gitee.com/uploads/images/2019/0415/214758_8c47b686_1203742.png "vue_login.png")

Grid:

![Grid](https://images.gitee.com/uploads/images/2019/0415/214815_c2dfdd1e_1203742.png "vue_bonuses.png")

多对多：

![多对多](https://images.gitee.com/uploads/images/2019/0415/220549_b19d2ca4_1203742.png "Vue_mtm.png")

编辑，下拉列表：

![输入图片说明](https://images.gitee.com/uploads/images/2019/0416/085420_45584d04_1203742.png "vue_update_dropdown.png")

#### 前端项目运行使用方法。
前端项目的使用，下载和平之翼代码生成器SHCEU版4.0.0 RC3，运行此代码生成器，生成前后端项目。运行后端项目。

将前端项目解压。如果没有安装Nodejs，请先安装。在解压的前端界面文件夹内运行 npm install命令。运行好后运行npm run dev

一切就绪后访问 http://localhost:8000/ 即可使用此示例。

### 贴心小工具
本版开始，随代码生成器附送一些开发中常用的贴心小工具，本版提供一个方便的代码替换工具。

截图

![输入图片说明](https://images.gitee.com/uploads/images/2019/0630/175828_7f2e7b7b_1203742.png "MF_tools.png")

### 和平之翼代码生成器已支持的功能 


1. 十余种单表操作 
1. 一对多关系 
1. 多重一对多关系，采用一对多别名实现 
1. 多对多关系，采用4种双表操作实现 
1. 多重多对多关系，采用多对多别名实现 
1. 标准生成器脚本（SGS）支持 
1. Excel代码生成支持 
1. 初始数据导入 
1. 缺省Excel数据导出 
1. id和DomainId两种格式主键支持 
1. delete和deleted删除标志自动反义功能 
1. MySQL/MariaDB支持 
1. Oracle支持 
1. 详细的编译警告和编译错提示 
1. 编译警告支持 
1. Eclipse JEE版兼容的代码生成物 
1. 整站代码生成 
1. 源文件或源代码自动保存 
1. 数据库脚本自动生成
1. 详细的在线理论文档 
1. 详细的用户手册和安装说明 
1. 丰富的代码示例 
1. EasyUI界面支持 
1. 已支持跨域以支持前后端分离，未来将直接生成前端项目
1. 新增在线问答文档
1. 支持前后端分离界面前端项目生成


### 交流QQ群
无垠式代码生成器群  277689737

### 千年隼 Millennium Falcon

![输入图片说明](https://images.gitee.com/uploads/images/2018/0713/140654_4c3e9483_1203742.jpeg "falcon.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0701/110221_9e31b7eb_1203742.jpeg "mf3.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0701/110014_1e638ddd_1203742.jpeg "mf2.jpg")

### 代码生成器截图

#### SGS生成界面

![输入图片说明](https://images.gitee.com/uploads/images/2019/0207/211700_676a3627_1203742.png "MF_sgs.png")

#### Excel生成界面

![输入图片说明](https://images.gitee.com/uploads/images/2019/0207/211711_223fe043_1203742.png "MF_Excel.png")

#### Excel模板

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/163937_8922952d_1203742.png "MF_ET_project'.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/163956_8de80351_1203742.png "MF_ET_Item.png")

#### 代码生成物

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/164017_21f22df4_1203742.png "MF_Grid.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/165201_7e66d0af_1203742.png "MF_Update.png")

#### 代码生成物：多对多

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/164031_7a83d6c8_1203742.png "MF_Mtm.png")

#### 代码生成物：下拉列表

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/164043_681d389e_1203742.png "MF_dropdown.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/164055_ca2f4daa_1203742.png "MF_dropdown2.png")

#### 最新的强大功能，Excel数据导出，数据导出结果截图：

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/165644_b79d8132_1203742.png "MF_EEX_bonuses.png")


### 和平之翼代码生成器图标，翅膀：

![输入图片说明](https://images.gitee.com/uploads/images/2018/1105/161512_b814baaa_1203742.jpeg "peacewing.jpg")

### 软件架构
软件架构说明

SHCEU技术栈，JQuery Easy UI, Spring MVC 4.2, Spring 4.2, Hibernate Criteria。

