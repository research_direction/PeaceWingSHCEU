package org.peacewing.core;

import org.peacewing.domain.Util;

public class BaseDaoGenerator extends Util{
	public BaseDaoGenerator(){
		super();
		super.fileName = "BaseDao.java";
	}
	
	public BaseDaoGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "BooleanUtil.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StringBuilder sb = new StringBuilder();
		sb.append("package "+this.getPackageToken()+".dao;\n");
		sb.append("\n");
		sb.append("import javax.annotation.Resource;\n");
		sb.append("import org.hibernate.SessionFactory;\n");
		sb.append("import org.springframework.orm.hibernate4.support.HibernateDaoSupport;\n\n");
		sb.append("public class BaseDao extends HibernateDaoSupport{\n");
		sb.append("@Resource\n");
		sb.append("public void setSessionFactory0(SessionFactory sessionFactory){\n");  
		sb.append("super.setSessionFactory(sessionFactory);\n");
		sb.append("}\n");
		sb.append("}\n");

		return sb.toString();
	}

}
