package org.peacewing.core;

import java.util.HashSet;

import org.peacewing.domain.Domain;
import org.peacewing.domain.Field;
import org.peacewing.domain.Method;
import org.peacewing.exception.ValidateException;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.TableStringUtil;

public class HibernateDomainDecorater {
	public static Domain generateDecroatedDomain(Domain domain) throws Exception{
		domain.addClassImports("javax.persistence.Column");
		domain.addClassImports("javax.persistence.Entity");
		domain.addClassImports("javax.persistence.GeneratedValue");
		domain.addClassImports("javax.persistence.Id");
		domain.addClassImports("javax.persistence.Table");
		domain.addClassImports("javax.persistence.UniqueConstraint");
		//domain.addClassImports("org.hibernate.annotations.Type");
		domain.addClassAnnotation("Entity");
		domain.addClassAnnotation("Table(name = \""+TableStringUtil.domainNametoLowerTableName(domain)+"\", uniqueConstraints = { @UniqueConstraint(columnNames = { \""+StringUtil.changeDomainFieldtoTableColum(domain.getDomainName().getFieldName())+"\" }) })");
		domain.setSetters(new HashSet<Method>());
		domain.setGetters(new HashSet<Method>());
		for (Field f:domain.getFields()){
			domain.generateSetterGetter(f);
		}
		for (Method m:domain.getGetters()){
			if (m.getLowerFirstMethodName().equals("get"+domain.getDomainId().getCapFirstFieldName())){				
				m.addMetaData("Column(name = \""+StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName())+"\")");
				m.addMetaData("GeneratedValue");
				m.addMetaData("Id");
			}else{
				m.addMetaData("Column(name=\""+StringUtil.changeDomainFieldtoTableColum(m.getStandardName().substring(3))+"\""+(m.getReturnType().generateTypeString().equals("String")?", length = 255 ":"")+")");	
//				if (m.getReturnType().getTypeName().equalsIgnoreCase("boolean")){	
//					m.addMetaData("org.hibernate.annotations.Type(type=\"yes_no\")");
//				}
			}
		}
		return domain;
	}
	
	public static String generateDecroatedDomainString(Domain domain) throws Exception{
		return generateDecroatedDomain(domain).generateClassString();
	}
}
