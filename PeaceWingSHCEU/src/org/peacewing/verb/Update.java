package org.peacewing.verb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Verb;
import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Dropdown;
import org.peacewing.domain.Field;
import org.peacewing.domain.JavascriptBlock;
import org.peacewing.domain.JavascriptMethod;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.easyui.EasyUIPositions;
import org.peacewing.generator.NamedStatementGenerator;
import org.peacewing.generator.NamedStatementListGenerator;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.MybatisSqlReflector;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class Update extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.hibernate.Session");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		method.addMetaData("Override");

		List<Writeable> list = new ArrayList<Writeable>();		
		list.add(new Statement(1000L,2, "Session s = this.getHibernateTemplate().getSessionFactory().getCurrentSession();"));
		list.add(new Statement(1000L,2,this.domain.getLowerFirstDomainName()+" = ("+this.domain.getStandardName()+") s.merge("+this.domain.getLowerFirstDomainName()+");"));
		list.add(new Statement(2000L,2,"s.saveOrUpdate("+this.domain.getLowerFirstDomainName()+");"));
		list.add(new Statement(3000L,2,"s.flush();"));
				
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return generateDaoImplMethod().generateMethodString();
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return generateDaoImplMethod().generateMethodContentStringWithSerial();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(2, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}
	

	@Override
	public String generateControllerMethodString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.springframework.transaction.annotation.Transactional");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		method.addMetaData("Override");
		method.addMetaData("Transactional");
		
		//Service method
		Method daomethod = this.generateDaoMethodDefinition();
				
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementListGenerator.generateServiceImplVoid(1000L, 2, InterVarUtil.DB.dao, daomethod));
		list.add(new Statement(2000L,2,"return true;"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public Update(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("Update"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("更新");
	}
	
	public Update(){
		super();
		this.setLabel("更新");
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateFacadeMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,this.domain.getLowerFirstDomainName(),this.domain.getType(),this.domain.getPackageToken(),"RequestBody"));
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		Method serviceMethod = this.generateServiceMethodDefinition();
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
		wlist.add(new Statement(2000L,2,service.getVarName()+"."+serviceMethod.getStandardName()+"("+this.domain.getLowerFirstDomainName()+");"));
		wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndNull(3000L, 2, resultMap));
		wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName()+";"));	
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateFacadeMethodString() throws Exception {
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception {
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setSerial(100);
		block.setStandardName("Update"+domain.getCapFirstDomainName());
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,0, "{"));
		sl.add(new Statement(2000,1, "text:'编辑',"));
		sl.add(new Statement(3000,1, "iconCls:'icon-edit',"));
		sl.add(new Statement(4000,1, "handler:function(){ "));
		sl.add(new Statement(5000,2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
		sl.add(new Statement(6000,2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
		sl.add(new Statement(7000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
		sl.add(new Statement(8000,3, "return;"));
		sl.add(new Statement(9000,2, "}"));	
		sl.add(new Statement(10000,2, "if (rows.length > 1) {"));
		sl.add(new Statement(11000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
		sl.add(new Statement(12000,3, "return;"));
		sl.add(new Statement(13000,2, "}"));
		sl.add(new Statement(13500,2, "$(\"#ffedit\").find(\"#"+this.domain.getDomainId().getLowerFirstFieldName()+"\").val(rows[0][\""+this.domain.getDomainId().getLowerFirstFieldName()+"\"]);"));
		long serial = 14000;
		for (Field f: domain.getFieldsWithoutIdAndActive()){
			if (f instanceof Dropdown){
				sl.add(new Statement(serial,2, "$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").combobox(\"setValue\",rows[0][\""+f.getLowerFirstFieldName()+"\"]);"));
			}else{
				sl.add(new Statement(serial,2, "$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").textbox(\"setValue\",rows[0][\""+f.getLowerFirstFieldName()+"\"]);"));
			}
			serial+=1000;
		}			
		sl.add(new Statement(serial,2, "var checkboxs = $(\"#ffedit\").find(\"input[name='active']\");"));
		sl.add(new Statement(serial+100,2, "for (var i=0;i<checkboxs.size();i++){"));
		sl.add(new Statement(serial+200,3, "if (checkboxs.get(i).value == \"\"+rows[0][\"active\"]) checkboxs.get(i).checked=true;"));
		sl.add(new Statement(serial+500,2, "}"));
		sl.add(new Statement(serial+1000,2, "$('#wupdate"+domain.getCapFirstDomainName()+"').window('open');"));
		sl.add(new Statement(serial+2000,1, "}"));
		sl.add(new Statement(serial+3000,0, "}"));
		block.setMethodStatementList(sl);
		return block;		
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		Domain domain = this.domain;
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("update"+domain.getCapFirstDomainName());
				
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "$.ajax({"));
		sl.add(new Statement(2000,2, "type: \"post\","));
		sl.add(new Statement(3000,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/update"+domain.getCapFirstDomainName()+"\","));
		sl.add(new Statement(4000,2, "data: JSON.stringify({"));
		long serial = 5000;
		for (Field f: domain.getFields()){
			if (f instanceof Dropdown){
				sl.add(new Statement(serial,3, f.getLowerFirstFieldName()+":$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").combobox(\"getValue\"),"));
			} else if (f.getFieldType().equalsIgnoreCase("boolean")){
				sl.add(new Statement(serial,3, f.getLowerFirstFieldName()+":parseBoolean($(\"#ffedit\").find(\"input[name='"+f.getLowerFirstFieldName()+"']:checked\").val()),"));	
			} else {
				sl.add(new Statement(serial,3, f.getLowerFirstFieldName()+":$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").val(),"));
			}
			serial+=1000;
		}
		sl.add(new Statement(serial,2, "}),"));
		sl.add(new Statement(serial+1000,2, "dataType: 'json',"));
		sl.add(new Statement(serial+2000,2, "contentType:\"application/json;charset=UTF-8\","));
		sl.add(new Statement(serial+3000,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(serial+4000,3, "if (data.success){"));
		sl.add(new Statement(serial+5000,4, "$('#ffedit').form('clear');"));
		sl.add(new Statement(serial+6000,4, "$(\"#ffedit\").find(\"input[name='"+domain.getActive().getLowerFirstFieldName()+"']\").get(0).checked = true;"));
		sl.add(new Statement(serial+7000,4, "$(\"#wupdate"+domain.getCapFirstDomainName()+"\").window('close');"));
		sl.add(new Statement(serial+8000,4, "$(\"#dg\").datagrid(\"load\");"));
		sl.add(new Statement(serial+9000,3, "}"));
		sl.add(new Statement(serial+10000,2, "},"));
		sl.add(new Statement(serial+11000,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(serial+12000,2, "},"));
		sl.add(new Statement(serial+13000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(serial+14000,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(serial+15000,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(serial+16000,2, "}"));
		sl.add(new Statement(serial+17000,1, "}); "));
		
		method.setMethodStatementList(sl);
		return method;
		
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}

}
