package org.peacewing.verb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Verb;
import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.generator.NamedStatementGenerator;
import org.peacewing.generator.NamedStatementListGenerator;
import org.peacewing.limitedverb.CountAllPage;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

//not used need fix!
public class ListActiveByPage extends Verb{
	protected CountAllPage countPage;	

	@Override
	public Method generateDaoImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("listActive"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("List",this.domain, this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.hibernate.Criteria");
		method.addAdditionalImport("org.hibernate.criterion.Restrictions");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.setThrowException(true);
		method.addMetaData("Override");
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,2, "Session s = this.getHibernateTemplate().getSessionFactory().getCurrentSession();"));
		list.add(new Statement(2000L,2,"Criteria criteria = s.createCriteria("+this.domain.getStandardName()+".class);"));
		list.add(new Statement(3000L,2,"criteria.add(Restrictions.eq(\""+this.domain.getActive().getLowerFirstFieldName()+"\","+this.domain.getDomainActiveStr()+"));"));
		list.add(new Statement(4000L,2,"List<"+this.domain.getStandardName()+"> "+StringUtil.lowerFirst(this.domain.getPlural())+" = (List<"+this.domain.getStandardName()+">)criteria.list();"));
		list.add(new Statement(5000L,2,"return "+StringUtil.lowerFirst(this.domain.getPlural())+";"));
		
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}
	
	@Override
	public String generateDaoImplMethodString() throws Exception{
		return generateDaoImplMethod().generateMethodString();
	}

	@Override
	public Method generateDaoMethodDefinition() {
		Method method = new Method();
		method.setStandardName("listActive"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("List",this.domain, this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		this.additionalImports.add(this.domain.getPackageToken()+"."+this.domain.getStandardName());
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception{
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateServiceMethodDefinition() {
		Method method = new Method();
		method.setStandardName("listActive"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("List",this.domain, this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.setThrowException(true);
		
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() {
		return null;
	}

	@Override
	public String generateControllerMethodString() {
		return generateControllerMethod().generateMethodString();
	}

	@Override
	public Method generateServiceImplMethod() {
		Method method = new Method();
		method.setStandardName("listActive"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("List",this.domain, this.domain.getPackageToken()));
		method.addAdditionalImport("java.sql.Connection");
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".database.DBConf");
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.setThrowException(true);
		
		Method daomethod = this.generateDaoMethodDefinition();
		
		List<Writeable> list = new ArrayList<Writeable>();
		//list.add(NamedStatementListGenerator.generateServiceImplReturnList(1000L, 2, InterVarUtil.DB.connection, InterVarUtil.DB.dbconf, this.domain, InterVarUtil.DB.dao, daomethod));
		method.setMethodStatementList(WriteableUtil.merge(list));

		return method;
	}

	@Override
	public String generateServiceImplMethodString() {
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public ListActiveByPage(){
		super();
		if (this.domain != null) this.setVerbName("ListActive"+StringUtil.capFirst(this.domain.getPlural()));
		else this.setVerbName("ListActive");
	}
	
	public ListActiveByPage(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("ListActive"+StringUtil.capFirst(this.domain.getPlural()));
	}

	@Override
	public String generateControllerMethodStringWithSerial() {
		return null;
	}

	@Override
	public Method generateFacadeMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("listAll"+StringUtil.capFirst(this.domain.getPlural())+"ByPage");
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		//method.addAdditionalImport(this.domain.getPackageToken()+".serviceimpl."+this.domain.getStandardName()+"ServiceImpl");
		method.addSignature(new Signature(1,"pagenum",new Type("Integer"), "","PathVariable"));
		method.addSignature(new Signature(2,"pagesize",new Type("Integer"),"","PathVariable"));
		method.addSignature(new Signature(3,"lastFlag",new Type("Boolean"),"","PathVariable"));
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"/{pagenum}/{pagesize}/{lastFlag}\", method = RequestMethod.GET)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		Var vlist = new Var(StringUtil.lowerFirst(this.domain.getStandardName())+"List", new Type("List",this.domain,this.domain.getPackageToken()));
		Method serviceMethod = this.generateServiceMethodDefinition();		
		Var pagesize = new Var("pagesize",new Type("int"),"10");
		Var pagenum = new Var("pagenum",new Type("int"),"1");
		Var pagecount = new Var("pagecount",new Type("int"));
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		Var lastFlag = new Var("lastFlag",new Type("boolean"));		
		wlist.add(new Statement(1000L,2,"if ("+pagesize.getVarName()+"==null || "+pagesize.getVarName()+" <= 0) "+pagesize.getVarName()+" = 10;"));
		wlist.add(NamedStatementGenerator.getJsonResultMap(2000L, 2, resultMap));
		wlist.add(new Statement(3000L,2,pagecount.generateTypeVarString() + " = " + service.getVarName()+"."+this.countPage.generateServiceMethodDefinition().getStandardCallString()+";"));
		wlist.add(new Statement(4000L,2,"if ("+pagenum.getVarName()+"==null || "+pagenum.getVarName() +" <= "+pagenum.getValue() +") " +pagenum.getVarName() +" = " + pagenum.getValue() +";"));
		wlist.add(new Statement(5000L,2,"if ("+pagenum.getVarName() +" >= "+pagecount.getVarName() +") " +pagenum.getVarName() +" = " + pagecount.getVarName() +";"));
		wlist.add(new Statement(5500L,2, "if ("+ lastFlag.getVarName() + ") "+pagenum.getVarName()+" = "+ pagecount.getVarName() +";"));
		wlist.add(new Statement(6000L,2, vlist.generateTypeVarString() + " = " +serviceMethod.generateStandardServiceImplCallString(InterVarUtil.DB.service.getVarName())+";"));
		wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndDomainListPaging(7000L, 2, resultMap,vlist,pagesize,pagenum,pagecount));
		wlist.add(new Statement(8000L, 2, "return " + resultMap.getVarName()+";"));	
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateFacadeMethodString() throws Exception{
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception{
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
}
