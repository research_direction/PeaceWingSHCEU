package org.peacewing.verb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Verb;
import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.JavascriptBlock;
import org.peacewing.domain.JavascriptMethod;
import org.peacewing.domain.Method;
import org.peacewing.domain.Statement;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.easyui.EasyUIPositions;
import org.peacewing.generator.NamedStatementGenerator;
import org.peacewing.generator.NamedStatementListGenerator;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.MybatisSqlReflector;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class ListAll extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("listAll"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("List",this.domain, this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.hibernate.Criteria");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.setThrowException(true);
		method.addMetaData("Override");
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,2, "Session s = this.getHibernateTemplate().getSessionFactory().getCurrentSession();"));
		list.add(new Statement(2000L,2,"Criteria criteria = s.createCriteria("+this.domain.getStandardName()+".class);"));
		list.add(new Statement(3000L,2,"List<"+this.domain.getStandardName()+"> "+StringUtil.lowerFirst(this.domain.getPlural())+" = (List<"+this.domain.getStandardName()+">)criteria.list();"));
		list.add(new Statement(4000L,2,"return "+StringUtil.lowerFirst(this.domain.getPlural())+";"));
		
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}
	
	@Override
	public String generateDaoImplMethodString() throws Exception{
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public Method generateDaoMethodDefinition() {
		Method method = new Method();
		method.setStandardName("listAll"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("List",this.domain, this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateDaoImplMethodStringWithSerial()  throws Exception{
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateServiceMethodDefinition() {
		Method method = new Method();
		method.setStandardName("listAll"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("List",this.domain, this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.setThrowException(true);
		
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() {
		return null;
	}

	@Override
	public String generateControllerMethodString() {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() {
		Method method = new Method();
		method.setStandardName("listAll"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("List",this.domain, this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.setThrowException(true);
		method.addMetaData("Override");
		
		Method daomethod = this.generateDaoMethodDefinition();
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementListGenerator.generateServiceImplReturnList(1000L, 2, this.domain, InterVarUtil.DB.dao, daomethod));
		method.setMethodStatementList(WriteableUtil.merge(list));

		return method;
	}

	@Override
	public String generateServiceImplMethodString() {
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public ListAll(){
		super();
		if (this.domain != null) this.setVerbName("ListAll"+StringUtil.capFirst(this.domain.getPlural()));
		else this.setVerbName("ListAll");
		this.setLabel("列出全部");
	}
	
	public ListAll(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("ListAll"+StringUtil.capFirst(this.domain.getPlural()));
		this.setLabel("列出全部");
	}

	@Override
	public String generateControllerMethodStringWithSerial() {
		return null;
	}

	@Override
	public Method generateFacadeMethod() {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.setIsprotected(false);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		Var vlist = new Var("list", new Type("List",this.domain,this.domain.getPackageToken()));
		Method serviceMethod = this.generateServiceMethodDefinition();
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
		wlist.add(new Statement(2000,2, vlist.generateTypeVarString() + " = " + service.getVarName()+"."+serviceMethod.getStandardName()+"();"));
		wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndDomainList(3000L, 2, resultMap,vlist));
		wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName()+";"));	

		method.setMethodStatementList(WriteableUtil.merge(wlist));		
		return method;
	}

	@Override
	public String generateFacadeMethodString() {
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() {
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}

}
