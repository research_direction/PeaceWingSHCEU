package org.peacewing.generator;

import org.peacewing.domain.Domain;
import org.peacewing.generator.DBDefinitionGenerator;

public class Oracle11gDBDefinitionGenerator extends DBDefinitionGenerator{

	public String generateDBSql(boolean createDB) throws Exception{
		StringBuilder sb = new StringBuilder();
			
		for (int i=0; i < this.getDomains().size();i++){
			Domain domain = this.getDomains().get(i);
			sb.append(Oracle11gSqlReflector.generateTableDefinition(domain)).append("\n");
		}
		return sb.toString();
	}
	
	public String generateDropTableSqls(boolean createDB) throws Exception{
		StringBuilder sb = new StringBuilder();
		for (int i=0; i < this.getDomains().size();i++){
			Domain domain = this.getDomains().get(i);
			sb.append(Oracle11gSqlReflector.generateDropTableStatement(domain)).append("\n");
		}
		sb.append("\n");
		return sb.toString();
	}
	
	public String generateDBSql() throws Exception{
		return generateDBSql(false);
	}
	
}
