package org.peacewing.generator;

import java.io.Serializable;

import org.peacewing.domain.Domain;
import org.peacewing.utils.SqlReflector;

public class MysqlTwoDomainsDBDefinitionGenerator implements Serializable,Comparable<MysqlTwoDomainsDBDefinitionGenerator> {
	private static final long serialVersionUID = 1053855118805402063L;
	protected Domain master;
	protected Domain slave;
	
	public MysqlTwoDomainsDBDefinitionGenerator(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
	}

	public String generateDBSql(String dbtype) throws Exception{
		StringBuilder sb = new StringBuilder();
		
		if ("Oracle".equalsIgnoreCase(dbtype)){
			sb.append(Oracle11gSqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
		}else{
			sb.append(SqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
		}
		return sb.toString();
	}

	@Override
	public int compareTo(MysqlTwoDomainsDBDefinitionGenerator o) {
		int result = this.master.compareTo(o.getMaster());
		if (result == 0) {
			result = this.slave.compareTo(o.getSlave());
		}
		return result;
	}	
	
	public String generateDropLinkTableSql() throws Exception{
		StringBuilder sb = new StringBuilder();

		sb.append(Oracle11gSqlReflector.generateDropLinkTableSql(this.master,this.slave)).append("\n");
		return sb.toString();
	}

	public Domain getMaster() {
		return master;
	}

	public void setMaster(Domain master) {
		this.master = master;
	}

	public Domain getSlave() {
		return slave;
	}

	public void setSlave(Domain slave) {
		this.slave = slave;
	}
}
