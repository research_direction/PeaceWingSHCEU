package org.peacewing.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Field;
import org.peacewing.domain.Method;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.limitedverb.CountAllPage;
import org.peacewing.utils.FieldUtil;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class NamedStatementListGenerator {
	
	public static StatementList getHibernateUpdateStatementList(long serial, int indent,Domain domain){
		StatementList list = new StatementList();
		list.add(new Statement(100L,indent, "Session s= this.getSessionFactory().openSession();"));
		list.add(new Statement(200L,indent,	"s.update("+domain.getLowerFirstDomainName()+");"));
		list.add(new Statement(200L,indent, "s.flush();"));
		list.setSerial(serial);
		return list;
	}


	public static StatementList generateSelectAllByPageQueryStatementList(long serial,int indent,Domain domain,Var pagesize, Var pagenum, Var service, CountAllPage countPage){
		try {
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(NamedStatementGenerator.getSelectAllByPageSqlStatement(1000L,indent,domain,InterVarUtil.DB.query));
			Var limitstart = new Var("limitstart",new Type("int"));
			Var limitcount = new Var("limitcount",new Type("int"));
			Var pagecount = new Var("pagecount",new Type("int"));
			countPage.setDomain(domain);
			list.add(new Statement(2000L,indent, limitstart.generateTypeVarString() + " = ("+ pagenum.getVarName() +"-1)*"+pagesize.getVarName()+";"));
			list.add(new Statement(3000L,indent, limitcount.generateTypeVarString() + " = " + pagesize.getVarName() + ";"));

			list.add(NamedStatementGenerator.getPrepareStatement(4000L,indent,InterVarUtil.DB.ps, InterVarUtil.DB.query, InterVarUtil.DB.connection));
			list.add(new Statement(5000L,indent, pagecount.generateTypeVarString() + " = " + "this."+countPage.getVerbName()+"("+InterVarUtil.DB.connection.getVarName()+","+pagesize.getVarName()+");"));
			list.add(new Statement(6000L,indent, "if ("+pagenum.getVarName()+ " <= 1) " +pagenum.getVarName() +" = 1;"));
			list.add(new Statement(7000L,indent, "if ("+pagenum.getVarName()+ " >= "+pagecount.getVarName()+ ") "+pagenum.getVarName() + " = " +pagecount.getVarName()+";"));

			list.add(NamedStatementListGenerator.generatePsSetDomainPagingFields(8000L, indent, domain, InterVarUtil.DB.ps,limitstart,limitcount));
			list.add(NamedStatementGenerator.getPrepareStatementExcuteQuery(9000L,indent, InterVarUtil.DB.result, InterVarUtil.DB.ps));
			list.add(NamedStatementGenerator.getPrepareDaomainArrayList(10000L,indent, domain));
			list.add(NamedStatementGenerator.getResultSetWhileNextLoopHead(11000L,indent, InterVarUtil.DB.result));  
			list.add(NamedStatementGenerator.getSingleLineComment(12000L,indent,"Build the list object."));
			list.add(NamedStatementGenerator.getPrepareDomainVarInit(13000L,indent, domain));
			list.add(NamedStatementListGenerator.generateSetDomainDataFromResultSet(14000L,indent,domain, InterVarUtil.DB.result));
			list.add(NamedStatementGenerator.getSingleLineComment(15000L,indent, "Build the object list."));			
			list.add(NamedStatementGenerator.getAddDomaintoList(16000L,indent, domain, InterVarUtil.Container.getList(domain)));
			list.add(NamedStatementGenerator.getLoopFooter(17000L,indent));
			list.add(NamedStatementGenerator.getReturnDomainList(18000,indent, domain, InterVarUtil.Container.getList(domain)));

			StatementList sList = WriteableUtil.merge(list);
			sList.setSerial(serial);
			return sList;
		} catch (Exception e){
			return null;
		}
	}
	
	public static StatementList generateCatchExceptionPrintStackReturnNullFooter(long serial, int indent){
		StatementList list = new StatementList();
		list.add(new Statement(100L,indent,"} catch (Exception e){"));
		list.add(new Statement(200L,indent,"e.printStackTrace();"));
		list.add(new Statement(300L,indent,"return null;"));
		list.add(new Statement(400L,indent,"}"));
		
		list.setSerial(serial);
		return list;
	}
	
	public static StatementList generateCatchExceptionPrintStackReturnEmptyListFooter(long serial, int indent,Domain domain){
		StatementList list = new StatementList();
		list.add(new Statement(100L,indent,"} catch (Exception e){"));
		list.add(new Statement(200L,indent,"e.printStackTrace();"));
		list.add(new Statement(300L,indent,"return new ArrayList<"+domain.getStandardName()+">();"));
		list.add(new Statement(400L,indent,"}"));
		
		list.setSerial(serial);
		return list;
	}
	
	public static StatementList generateCatchExceptionPrintStackReturnOneFooter(long serial, int indent,Domain domain){
		StatementList list = new StatementList();
		list.add(new Statement(100L,indent,"} catch (Exception e){"));
		list.add(new Statement(200L,indent,"e.printStackTrace();"));
		list.add(new Statement(300L,indent,"return 1;"));
		list.add(new Statement(400L,indent,"}"));
		
		list.setSerial(serial);
		return list;
	}
	
	public static StatementList generateCatchExceptionPrintStackFooter(long serial, int indent){
		StatementList list = new StatementList();
		list.add(new Statement(100L,indent,"} catch (Exception e){"));
		list.add(new Statement(200L,indent,"e.printStackTrace();"));
		list.add(new Statement(300L,indent,"}"));
		
		list.setSerial(serial);
		return list;
	}
	
	public static StatementList generateCatchExceptionPrintStackRedirectUrlFinallyCloseOutFooter(long serial, int indent, Var response, String url,Var out){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(100L,indent,"} catch (Exception e){"));
		list.add(new Statement(200L,indent,"e.printStackTrace();"));
		list.add(new Statement(300L,indent,response.getVarName()+".sendRedirect(\""+url+"\");"));
		list.add(new Statement(400L,indent,"} finally {"));
		list.add(new Statement(500L,indent,out.getVarName()+".close();"));
		list.add(new Statement(400L,indent,"}"));
		return list;
	}
	
	public static StatementList generateSetDomainDataFromResultSet(long serial, int indent, Domain domain, Var resultSet){
		StatementList list = new StatementList();
		list.setSerial(serial);
		long fieldSerial = 100L;
		for (Field f : domain.getFields()){
			list.add(new Statement(fieldSerial,indent,StringUtil.lowerFirst(domain.getStandardName())+".set"+StringUtil.capFirst(f.getFieldName())+"("+resultSet.getVarName()+".get"+StringUtil.capFirst(f.getFieldType())+"(\""+f.getFieldTableColumName()+"\"));"));
			fieldSerial += 100L;
		}
		return list;
	}
	
	public static StatementList generateSetDomainDataFromRequest(long serial, int indent, Domain domain, Var request){
		StatementList list = new StatementList();
		list.setSerial(serial);
		long fieldSerial = 100L;
		for (Field f : domain.getFields()){
			list.add(new Statement(fieldSerial,indent,StringUtil.lowerFirst(domain.getStandardName())+".set"+StringUtil.capFirst(f.getFieldName())+"("+FieldUtil.generateRequestGetParameterString(f, request)+");"));
			fieldSerial += 100L;
		}
		return list;
	}
	
	public static StatementList generateSetDomainDataFromRequestWithoutDomainId(long serial, int indent, Domain domain, Var request){
		StatementList list = new StatementList();
		list.setSerial(serial);
		long fieldSerial = 100L;
		for (Field f : domain.getFieldsWithoutId()){
			list.add(new Statement(fieldSerial,indent,StringUtil.lowerFirst(domain.getStandardName())+".set"+StringUtil.capFirst(f.getFieldName())+"("+FieldUtil.generateRequestGetParameterString(f, request)+");"));
			fieldSerial += 100L;
		}
		return list;
	}

	public static StatementList generatePsSetDomainFields(long serial, int indent, Domain domain, Var ps){
		StatementList list = new StatementList();
		list.setSerial(serial);
		long fieldSerial = 100L;
		int psFieldSerial = 1;
		List<Field> fieldList = new ArrayList<Field>();
		fieldList.addAll(domain.getFields());
		Collections.sort(fieldList);
		for (Field f : fieldList){
			list.add(new Statement(fieldSerial,indent,StringUtil.lowerFirst(ps.getVarName())+".set"+StringUtil.capFirst(f.getFieldType())+"("+psFieldSerial+","+StringUtil.lowerFirst(domain.getStandardName())+".get"+StringUtil.capFirst(f.getFieldName())+"());"));
			fieldSerial += 100L;
			psFieldSerial += 1;
		}
		return list;
	}
	
	public static StatementList generatePsSetDomainFieldsWithoutId(long serial, int indent, Domain domain, Var ps){
		StatementList list = new StatementList();
		list.setSerial(serial);
		long fieldSerial = 100L;
		int psFieldSerial = 1;
		List<Field> fieldList = new ArrayList<Field>();
		fieldList.addAll(domain.getFieldsWithoutId());
		Collections.sort(fieldList);
		for (Field f : fieldList){
			list.add(new Statement(fieldSerial,indent,StringUtil.lowerFirst(ps.getVarName())+".set"+StringUtil.capFirst(f.getFieldType())+"("+psFieldSerial+","+StringUtil.lowerFirst(domain.getStandardName())+".get"+StringUtil.capFirst(f.getFieldName())+"());"));
			fieldSerial += 100L;
			psFieldSerial += 1;
		}
		return list;
	}
	
	public static StatementList generatePsSetDomainPagingFields(long serial, int indent, Domain domain, Var ps, Var limitstart, Var limitcount){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(1000L,indent,StringUtil.lowerFirst(ps.getVarName())+".setInt(1,"+limitstart.getVarName()+");"));
		list.add(new Statement(2000L,indent,StringUtil.lowerFirst(ps.getVarName())+".setInt(2,"+limitcount.getVarName()+");"));
		return list;
	}
	
	public static StatementList generateResultReturnSuccess(long serial, int indent, Var result){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(100L,indent, "if ("+result.getVarName()+" > 0) {"));
		list.add(new Statement(100L,indent, "return true;"));
		list.add(new Statement(100L,indent, "}"));
		list.add(new Statement(100L,indent, "return false;"));
		return list;
	}
	
	public static StatementList generateServiceImplVoid(long serial,int indent, Var dao, Method daoVoidFunction){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(200L,indent,daoVoidFunction.generateStandardDaoImplCallString(dao.getVarName())+";"));
		return list;
	}
	
	public static StatementList generateServiceImplReturnBoolean(long serial,int indent, Var dao, Method daoVoidFunction){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(200L,indent,"return "+daoVoidFunction.generateStandardDaoImplCallString(dao.getVarName())+";"));
		return list;
	}
	
	public static StatementList generateServiceImplReturnList(long serial,int indent,  Domain domain, Var dao, Method daoReturnListFunction){
		StatementList list = new StatementList();
		list.setSerial(serial);

		list.add(new Statement(100L,indent,"return " + daoReturnListFunction.generateStandardServiceImplCallString(dao.getVarName())+";"));
		return list;
	}
	
	public static StatementList generateServiceImplVoid(long serial,int indent,  Domain domain, Var dao, Method daoReturnListFunction){
		StatementList list = new StatementList();
		list.setSerial(serial);

		list.add(new Statement(100L,indent,daoReturnListFunction.generateStandardServiceImplCallString(dao.getVarName())+";"));
		return list;
	}

	public static StatementList generateServiceImplReturnDomain(long serial,int indent, Var connection, Var dbconf, Domain domain, Var dao, Method daoBooleanFunction){
		StatementList list = new StatementList();
		list.setSerial(serial);

		list.add(NamedStatementGenerator.getDBConfInitDBAutoRelease(100L, indent, connection, dbconf));
		list.add(new Statement(200L,indent, "\treturn " + daoBooleanFunction.generateStandardServiceImplCallString(dao.getVarName())+";"));
		list.add(NamedStatementGenerator.getLoopFooter(300L, indent));
		return list;
	}
	
	public static StatementList generateServiceImplReturnInt(long serial,int indent, Var connection, Var dbconf, Domain domain, Var dao, Method daoReturnIntFunction){
		StatementList list = new StatementList();
		list.setSerial(serial);

		list.add(NamedStatementGenerator.getDBConfInitDBAutoRelease(100L, indent, connection, dbconf));
		list.add(new Statement(200L,indent, "return " + daoReturnIntFunction.generateStandardServiceImplCallString(dao.getVarName())+";"));
		list.add(NamedStatementGenerator.getLoopFooter(300L, indent));
		return list;
	}
	
	public static StatementList getPutJsonResultMapWithSuccessAndNull(long serial,int indent, Var map){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(100L,indent,map.getVarName() +".put(\"success\",true);"));
		list.add(new Statement(200L,indent,map.getVarName() +".put(\"data\",null);"));
		return list;
	}
	
	public static StatementList getPutJsonResultMapWithSuccessAndDomainList(long serial,int indent, Var map,Var domainList){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(100L,indent,map.getVarName() +".put(\"success\",true);"));
		list.add(new Statement(100L,indent,map.getVarName() +".put(\"rows\","+domainList.getVarName()+");"));
		return list;
	}
	
	public static StatementList getPutJsonResultMapWithSuccessAndDomainVar(long serial,int indent, Var map,Var domainVar){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(100L,indent,map.getVarName() +".put(\"success\",true);"));
		list.add(new Statement(200L,indent,map.getVarName() +".put(\"data\","+domainVar.getVarName()+");"));
		return list;
	}
	
	public static StatementList getPutJsonResultMapWithSuccessAndDomainListPaging(long serial,int indent, Var map,Var domainList, Var pagesize, Var pagenum, Var pagecount){
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.add(new Statement(100L,indent,map.getVarName() +".put(\"success\",true);"));
		list.add(new Statement(200L,indent,map.getVarName() +".put(\"data\","+domainList.getVarName()+");"));
		list.add(new Statement(300L,indent,map.getVarName() +".put(\""+pagesize.getVarName()+"\","+pagesize.getVarName()+");"));
		list.add(new Statement(400L,indent,map.getVarName() +".put(\""+pagenum.getVarName()+"\","+pagenum.getVarName()+");"));
		list.add(new Statement(500L,indent,map.getVarName() +".put(\""+pagecount.getVarName()+"\","+pagecount.getVarName()+");"));
		return list;
	}
}
