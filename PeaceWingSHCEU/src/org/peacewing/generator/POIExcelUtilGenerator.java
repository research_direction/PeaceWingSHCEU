package org.peacewing.generator;

import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.domain.Util;

public class POIExcelUtilGenerator extends Util{
	public POIExcelUtilGenerator(){
		super();
		super.fileName = "POIExcelUtil.java";
	}
	
	public POIExcelUtilGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "POIExcelUtil.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StatementList sList = new StatementList();
		sList.add(new Statement(500L,0, "package "+this.packageToken+".utils;"));
		sList.add(new Statement(1000L,0,"import java.awt.Color;"));
		sList.add(new Statement(2000L,0,"import java.io.File;"));
		sList.add(new Statement(3000L,0,"import java.io.FileOutputStream;"));
		sList.add(new Statement(4000L,0,"import java.io.OutputStream;"));
		sList.add(new Statement(5000L,0,"import java.util.List;"));
		sList.add(new Statement(6000L,0,""));
		sList.add(new Statement(7000L,0,"import org.apache.commons.io.FileUtils;"));
		sList.add(new Statement(8000L,0,"import org.apache.poi.hssf.usermodel.HSSFCell;"));
		sList.add(new Statement(9000L,0,"import org.apache.poi.hssf.usermodel.HSSFCellStyle;"));
		sList.add(new Statement(10000L,0,"import org.apache.poi.hssf.usermodel.HSSFFont;"));
		sList.add(new Statement(11000L,0,"import org.apache.poi.hssf.usermodel.HSSFPalette;"));
		sList.add(new Statement(12000L,0,"import org.apache.poi.hssf.usermodel.HSSFRow;"));
		sList.add(new Statement(13000L,0,"import org.apache.poi.hssf.usermodel.HSSFSheet;"));
		sList.add(new Statement(14000L,0,"import org.apache.poi.hssf.usermodel.HSSFWorkbook;"));
		sList.add(new Statement(15000L,0,"import org.apache.poi.ss.usermodel.BorderStyle;"));
		sList.add(new Statement(16000L,0,"import org.apache.poi.ss.usermodel.Cell;"));
		sList.add(new Statement(17000L,0,"import org.apache.poi.ss.usermodel.ClientAnchor;"));
		sList.add(new Statement(18000L,0,"import org.apache.poi.ss.usermodel.CreationHelper;"));
		sList.add(new Statement(19000L,0,"import org.apache.poi.ss.usermodel.Drawing;"));
		sList.add(new Statement(20000L,0,"import org.apache.poi.ss.usermodel.FillPatternType;"));
		sList.add(new Statement(21000L,0,"import org.apache.poi.ss.usermodel.HorizontalAlignment;"));
		sList.add(new Statement(22000L,0,"import org.apache.poi.ss.usermodel.VerticalAlignment;"));
		sList.add(new Statement(23000L,0,"import org.apache.poi.ss.usermodel.Workbook;"));
		sList.add(new Statement(24000L,0,"import org.apache.poi.ss.util.CellRangeAddress;"));
		sList.add(new Statement(25000L,0,""));
		sList.add(new Statement(26000L,0,"public final class POIExcelUtil {"));
		sList.add(new Statement(27000L,1,"public static void exportExcelSheet(OutputStream out,String sheetName,List<String> headers, List<List<String>> contents) throws Exception{"));
		sList.add(new Statement(28000L,2,"HSSFWorkbook wb = new HSSFWorkbook();"));
		sList.add(new Statement(29000L,2,"HSSFSheet sheet = wb.createSheet(sheetName);"));
		sList.add(new Statement(30000L,2,"HSSFRow row;"));
		sList.add(new Statement(31000L,2,"HSSFCell cell;"));
		sList.add(new Statement(32000L,2,""));
		sList.add(new Statement(33000L,2,"short colorIndex = 10;"));
		sList.add(new Statement(34000L,2,"HSSFPalette palette = wb.getCustomPalette();"));
		sList.add(new Statement(35000L,2,"Color rgb = Color.YELLOW;"));
		sList.add(new Statement(36000L,2,"short bgIndex = colorIndex ++;"));
		sList.add(new Statement(37000L,2,"palette.setColorAtIndex(bgIndex, (byte) rgb.getRed(), (byte) rgb.getGreen(), (byte) rgb.getBlue());"));
		sList.add(new Statement(38000L,2,"short bdIndex = colorIndex ++;"));
		sList.add(new Statement(39000L,2,"rgb = Color.BLACK;"));
		sList.add(new Statement(40000L,2,"palette.setColorAtIndex(bdIndex, (byte) rgb.getRed(), (byte) rgb.getGreen(), (byte) rgb.getBlue());"));
		sList.add(new Statement(41000L,2,""));
		sList.add(new Statement(42000L,2,"HSSFCellStyle cellStyle = wb.createCellStyle();"));
		sList.add(new Statement(43000L,2,"cellStyle.setBorderBottom(BorderStyle.THIN);"));
		sList.add(new Statement(44000L,2,"cellStyle.setBorderLeft(BorderStyle.THIN);"));
		sList.add(new Statement(45000L,2,"cellStyle.setBorderTop(BorderStyle.THIN);"));
		sList.add(new Statement(46000L,2,"cellStyle.setBorderRight(BorderStyle.THIN);"));
		sList.add(new Statement(47000L,2,"//bdIndex 边框颜色下标值"));
		sList.add(new Statement(48000L,2,"cellStyle.setBottomBorderColor(bdIndex);"));
		sList.add(new Statement(49000L,2,"cellStyle.setLeftBorderColor(bdIndex);"));
		sList.add(new Statement(50000L,2,"cellStyle.setRightBorderColor(bdIndex);"));
		sList.add(new Statement(51000L,2,"cellStyle.setTopBorderColor(bdIndex);"));
		sList.add(new Statement(52000L,2,""));
		sList.add(new Statement(53000L,2,"cellStyle.setAlignment(HorizontalAlignment.CENTER);"));
		sList.add(new Statement(54000L,2,"cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);"));
		sList.add(new Statement(55000L,2,""));
		sList.add(new Statement(56000L,2,"HSSFCellStyle cellHeaderStyle = wb.createCellStyle();"));
		sList.add(new Statement(57000L,2,"cellHeaderStyle.cloneStyleFrom(cellStyle);"));
		sList.add(new Statement(58000L,2,""));
		sList.add(new Statement(59000L,2,"cellHeaderStyle.setFillForegroundColor(bgIndex); //bgIndex 背景颜色下标值"));
		sList.add(new Statement(60000L,2,"cellHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);"));
		sList.add(new Statement(61000L,2,""));
		sList.add(new Statement(62000L,2,"writeRow(wb,sheet,bgIndex,bdIndex,1,cellHeaderStyle,headers);"));
		sList.add(new Statement(63000L,0,""));
		sList.add(new Statement(64000L,2,"for(int i = 0; i < contents.size(); i++) {"));
		sList.add(new Statement(65000L,3,"writeRow(wb,sheet,bgIndex,bdIndex,i+2,cellStyle,contents.get(i));"));
		sList.add(new Statement(66000L,2,"}"));
		sList.add(new Statement(67000L,2,""));
		sList.add(new Statement(68000L,2,"//创建表格之后设置行高与列宽"));
		sList.add(new Statement(69000L,2,"for(int i = 1; i < contents.size()+2; i++) {"));
		sList.add(new Statement(70000L,3,"row = sheet.getRow(i);"));
		sList.add(new Statement(71000L,3,"row.setHeightInPoints(30);"));
		sList.add(new Statement(72000L,2,"}"));
		sList.add(new Statement(73000L,2,"for(int j = 1; j < headers.size()+1; j++) {"));
		sList.add(new Statement(74000L,3,"sheet.setColumnWidth(j, MSExcelUtil.pixel2WidthUnits(120));"));
		sList.add(new Statement(75000L,2,"}"));
		sList.add(new Statement(76000L,2,"wb.write(out);"));
		sList.add(new Statement(77000L,1,"}"));
		sList.add(new Statement(78000L,1,""));
		sList.add(new Statement(79000L,1,"protected static void writeRow(HSSFWorkbook wb,HSSFSheet sheet,short bgIndex,short bdIndex,int rowIndex,HSSFCellStyle cellStyle,List<String> data) {"));
		sList.add(new Statement(80000L,2,"HSSFRow row = sheet.createRow(rowIndex);//创建表格行"));
		sList.add(new Statement(81000L,2,"for(int j = 0; j < data.size(); j++) {"));
		sList.add(new Statement(82000L,3,"Cell cell = row.createCell(j+1);//根据表格行创建单元格"));
		sList.add(new Statement(83000L,3,"cell.setCellStyle(cellStyle);"));
		sList.add(new Statement(84000L,3,"cell.setCellValue(String.valueOf(StringUtil.nullTrim(data.get(j))));"));
		sList.add(new Statement(85000L,2,"}"));
		sList.add(new Statement(86000L,1,"}"));
		sList.add(new Statement(87000L,0,""));	
		sList.add(new Statement(88000L,0,"}"));
		return sList.getContent();
	}

}
