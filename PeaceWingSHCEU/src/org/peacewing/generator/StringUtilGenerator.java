package org.peacewing.generator;

import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.domain.Util;

public class StringUtilGenerator extends Util{
	public StringUtilGenerator(){
		super();
		super.fileName = "StringUtil.java";
	}
	
	public StringUtilGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "StringUtil.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StatementList sList = new StatementList();
		sList.add(new Statement(500L,"package "+this.getPackageToken()+".utils;"));
		sList.add(new Statement(1000L,0,"public class StringUtil {"));
		sList.add(new Statement(2000L,1,"public static boolean isBlank(Object o) {"));
		sList.add(new Statement(3000L,2,"if (o == null || \"\".equals(o))"));
		sList.add(new Statement(4000L,3,"return true;"));
		sList.add(new Statement(5000L,2,"else"));
		sList.add(new Statement(6000L,3,"return false;"));
		sList.add(new Statement(7000L,1,"}"));
		sList.add(new Statement(8000L,0,""));
		sList.add(new Statement(9000L,1,"public static String nullTrim(String str) {"));
		sList.add(new Statement(10000L,2,"return str == null || str.trim().length() == 0 || \"null\".equals(str) ? \"\" : str;"));
		sList.add(new Statement(11000L,1,"}"));
		sList.add(new Statement(12000L,0,"}"));

		return sList.getContent();
	}

}
