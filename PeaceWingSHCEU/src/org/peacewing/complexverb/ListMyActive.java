package org.peacewing.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.Type;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class ListMyActive extends TwoDomainVerb{

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport("java.util.TreeSet");
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addAdditionalImport(this.linkDomain.getPackageToken()+".domain."+this.linkDomain.getStandardName());
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id",this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		method.addMetaData("Override");
		
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,2,"Session s = this.getHibernateTemplate().getSessionFactory().getCurrentSession();"));
		sList.add(new Statement(2000L,2,"Criteria criteria = s.createCriteria("+this.linkDomain.getStandardName()+".class);"));
		sList.add(new Statement(3000L,2,"criteria.add(Restrictions.eq(\""+this.master.getLowerFirstDomainName()+"."+this.master.getDomainId().getLowerFirstFieldName()+"\", "+this.master.getLowerFirstDomainName()+"Id));"));
		sList.add(new Statement(4000L,2,"List<"+this.linkDomain.getStandardName()+"> "+this.getLinkDomain().getLowerFirstPlural()+" = (List<"+this.linkDomain.getStandardName()+">)criteria.list();"));
		sList.add(new Statement(5000L,2,"Set<"+this.slave.getStandardName()+"> retVals = new TreeSet<>();"));
		sList.add(new Statement(6000L,2,"for ("+this.linkDomain.getStandardName()+" "+this.linkDomain.getLowerFirstTwoChar()+":"+this.getLinkDomain().getLowerFirstPlural()+") {"));
		sList.add(new Statement(7000L,3,"if ("+this.linkDomain.getLowerFirstTwoChar()+".get"+this.slave.getAliasOrName()+"().get"+this.slave.getActive().getCapFirstFieldName()+"()=="+this.slave.getDomainActiveStr()+") {"));
		sList.add(new Statement(8000L,4,"retVals.add("+this.linkDomain.getLowerFirstTwoChar()+".get"+this.slave.getAliasOrName()+"());"));
		sList.add(new Statement(9000L,3,"}"));
		sList.add(new Statement(10000L,2,"}"));
		sList.add(new Statement(11000L,2,"return retVals;"));

		
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id",this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id", this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));		
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id", this.master.getDomainId().getClassType()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport("java.util.TreeSet");
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addAdditionalImport(this.master.getPackageToken()+".dao."+this.master.getStandardName()+"Dao");
		method.addAdditionalImport(this.slave.getPackageToken()+".service."+this.slave.getStandardName()+"Service");
		method.setThrowException(true);
		method.addMetaData("Override");
		
		Method daomethod = this.generateDaoMethodDefinition();
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,2,"Set<"+this.slave.getCapFirstDomainName()+"> set = new TreeSet<"+this.slave.getStandardName()+">();"));
		list.add(new Statement(2000L,2,"set.addAll(dao."+StringUtil.lowerFirst(this.getVerbName())+"("+this.master.getLowerFirstDomainName()+"Id"+"));"));
		list.add(new Statement(3000L,2,"return set;"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateFacadeMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));		
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("java.util.Set");
		
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addAdditionalImport(this.master.getPackageToken()+".service."+this.master.getStandardName()+"Service");
		method.addAdditionalImport(this.master.getPackageToken()+".serviceimpl."+this.master.getStandardName()+"ServiceImpl");
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id",this.master.getDomainId().getClassType(), this.slave.getPackageToken(),"RequestParam (required = false)"));
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		wlist.add(new Statement(1000L,2,"Map<String,Object> result = new TreeMap<String,Object>();"));
		wlist.add(new Statement(2000L,2,"Set<"+this.slave.getCapFirstDomainName()+"> set = new TreeSet<"+this.slave.getCapFirstDomainName()+">();"));
		wlist.add(new Statement(3000L,2,"if ("+this.master.getLowerFirstDomainName()+"Id"+"!=null) set = service."+StringUtil.lowerFirst(this.getVerbName())+"("+this.master.getLowerFirstDomainName()+"Id"+");"));
		wlist.add(new Statement(3000L,2,"result.put(\"success\",true);"));
		wlist.add(new Statement(4000L,2,"result.put(\"rows\",set);"));
		wlist.add(new Statement(5000L,2,"return result;"));	
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateFacadeMethodString() throws Exception {
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception {
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	public ListMyActive(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		this.setVerbName("ListActive"+this.master.getCapFirstDomainName()+StringUtil.capFirst(this.slave.getAliasPlural())+"Using"+this.master.getCapFirstDomainName()+"Id");
		this.setLabel("列出所属");
	}

}
