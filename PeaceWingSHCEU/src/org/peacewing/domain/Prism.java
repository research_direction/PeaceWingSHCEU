package org.peacewing.domain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.peacewing.core.Action;
import org.peacewing.core.HibernateDomainDecorater;
import org.peacewing.core.SpringMVCFacade;
import org.peacewing.core.Verb;
import org.peacewing.easyui.EasyUIManyToManyTemplate;
import org.peacewing.easyui.EasyUIPageTemplate;
import org.peacewing.exception.ValidateException;
import org.peacewing.generator.DBDefinitionGenerator;
import org.peacewing.generator.JsonPagingGridJspTemplate;
import org.peacewing.generator.JspTemplate;
import org.peacewing.generator.MybatisDaoXmlDecorator;
import org.peacewing.generator.NamedUtilMethodGenerator;
import org.peacewing.limitedverb.CountActiveRecords;
import org.peacewing.limitedverb.CountAllRecords;
import org.peacewing.limitedverb.CountSearchByFieldsRecords;
import org.peacewing.limitedverb.DaoOnlyVerb;
import org.peacewing.limitedverb.NoControllerVerb;
import org.peacewing.utils.StringUtil;
import org.peacewing.verb.Activate;
import org.peacewing.verb.ActivateAll;
import org.peacewing.verb.Add;
import org.peacewing.verb.Delete;
import org.peacewing.verb.DeleteAll;
import org.peacewing.verb.Export;
import org.peacewing.verb.FindById;
import org.peacewing.verb.FindByName;
import org.peacewing.verb.ListActive;
import org.peacewing.verb.ListAll;
import org.peacewing.verb.SearchByFieldsByPage;
import org.peacewing.verb.SoftDelete;
import org.peacewing.verb.SoftDeleteAll;
import org.peacewing.verb.Toggle;
import org.peacewing.verb.ToggleOne;
import org.peacewing.verb.Update;

import net.sf.json.JSONObject;

public class Prism implements Comparable {
	protected long prismId;
	protected String standardName;
	protected long namingId;
	protected Naming naming;
	protected long domainClassId;
	protected Domain domain;
	protected long daoimplClassId;
	protected long serviceimplClassId;
	protected long daoId;
	protected Dao dao;
	protected DaoImpl daoimpl;
	protected long serviceId;
	protected Service service;
	protected ServiceImpl serviceimpl;
	protected String prismComment;
	protected List<Class> classes = new ArrayList<Class>();
	protected List<Util> utils = new ArrayList<Util>();
	protected List<Controller> controllers = new ArrayList<Controller>();
	protected List<SpringMVCFacade> facades = new ArrayList<SpringMVCFacade>();
	protected String folderPath = "D:/JerryWork/Infinity/testFiles/";
	protected DBDefinitionGenerator dbDefinitionGenerator;
	protected List<JspTemplate> jsptemplates = new ArrayList<JspTemplate>();
	protected List<JsonPagingGridJspTemplate> jsonjsptemplates = new ArrayList<JsonPagingGridJspTemplate>();
	protected String packageToken;
	protected TestSuite prismTestSuite;
	protected TestCase daoImplTestCase;
	protected TestCase serviceImplTestCase;
	protected List<Verb> verbs = new ArrayList<Verb>();
	protected List<NoControllerVerb> noControllerVerbs = new ArrayList<NoControllerVerb>();
	protected List<DaoOnlyVerb> daoOnlyVerbs = new ArrayList<DaoOnlyVerb>();
	protected MybatisDaoXmlDecorator mybatisDaoXmlDecorator;
	protected Action action;
	protected String label;
	protected Set<Pair> manyToManySlaveNames = new TreeSet<Pair>();
	protected Set<ManyToMany> manyToManies = new TreeSet<ManyToMany>();
	protected Set<Domain> projectDomains = new TreeSet<Domain>();
	protected SpringMVCFacade facade;
	protected String title = "";
	protected String subTitle = "";
	protected String footer = "";
	protected String crossOrigin = "";	
	protected String resolution = "low";
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public Prism() {
		super();
	}
	

	public SpringMVCFacade getFacade() {
		return facade;
	}

	public void setFacade(SpringMVCFacade facade) {
		this.facade = facade;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public MybatisDaoXmlDecorator getMybatisDaoXmlDecorator() {
		return mybatisDaoXmlDecorator;
	}

	public void setMybatisDaoXmlDecorator(MybatisDaoXmlDecorator mybatisDaoXmlDecorator) {
		this.mybatisDaoXmlDecorator = mybatisDaoXmlDecorator;
	}

	public List<SpringMVCFacade> getFacades() {
		return facades;
	}

	public void setFacades(List<SpringMVCFacade> facades) {
		this.facades = facades;
	}

	public List<NoControllerVerb> getNoControllerVerbs() {
		return noControllerVerbs;
	}

	public void setNoControllerVerbs(List<NoControllerVerb> noControllerVerbs) {
		this.noControllerVerbs = noControllerVerbs;
	}

	public List<DaoOnlyVerb> getDaoOnlyVerbs() {
		return daoOnlyVerbs;
	}

	public void setDaoOnlyVerbs(List<DaoOnlyVerb> daoOnlyVerbs) {
		this.daoOnlyVerbs = daoOnlyVerbs;
	}

	public List<Util> getUtils() {
		return utils;
	}

	public void setUtils(List<Util> utils) {
		this.utils = utils;
	}

	public void addUtil(Util util) {
		this.utils.add(util);
	}

	public long getPrismId() {
		return prismId;
	}

	public List<Class> getClasses() {
		return classes;
	}

	public void setClasses(List<Class> classes) {
		this.classes = classes;
	}

	public void addClass(Class clazz) {
		this.classes.add(clazz);
	}

	public void setPrismId(long prismId) {
		this.prismId = prismId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public long getNamingId() {
		return namingId;
	}

	public void setNamingId(long namingId) {
		this.namingId = namingId;
	}

	public Naming getNaming() {
		return naming;
	}

	public void setNaming(Naming naming) {
		this.naming = naming;
	}

	public long getDomainClassId() {
		return domainClassId;
	}

	public void setDomainClassId(long domainClassId) {
		this.domainClassId = domainClassId;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public long getDaoimplClassId() {
		return daoimplClassId;
	}

	public void setDaoimplClassId(long daoimplClassId) {
		this.daoimplClassId = daoimplClassId;
	}

	public DaoImpl getDaoimpl() {
		return this.daoimpl;
	}

	public void setDaoimpl(DaoImpl daoimpl) {
		this.daoimpl = daoimpl;
	}

	public long getServiceImplClassId() {
		return serviceimplClassId;
	}

	public void setServiceImplClassId(long serviceimplClassId) {
		this.serviceimplClassId = serviceimplClassId;
	}

	public ServiceImpl getServiceImpl() {
		return serviceimpl;
	}

	public void setServiceImpl(ServiceImpl serviceimpl) {
		this.serviceimpl = serviceimpl;
	}

	public long getDaoId() {
		return daoId;
	}

	public void setDaoId(long daoId) {
		this.daoId = daoId;
	}

	public Dao getDao() {
		return dao;
	}

	public void setDao(Dao dao) {
		this.dao = dao;
	}

	public long getServiceId() {
		return serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public String getPrismComment() {
		return prismComment;
	}

	public void setPrismComment(String prismComment) {
		this.prismComment = prismComment;
	}

	public void generatePrismFiles() throws ValidateException {
		ValidateInfo info = this.validate();
		if (info.success() == false) {
			ValidateException e = new ValidateException(info);
			throw e;
		}
		try {
			String srcfolderPath = folderPath;
			if (this.packageToken != null && !"".equals(this.packageToken)){
				srcfolderPath = folderPath + "src/" + packagetokenToFolder(this.packageToken);

				writeToFile(srcfolderPath + "domain/" + StringUtil.capFirst(this.getDomain().getStandardName()) + ".java",
					this.getDomain().generateClassString());
			
				if (this.getDao() != null) {
					writeToFile(
							srcfolderPath + "dao/" + StringUtil.capFirst(this.getDomain().getStandardName()) + "Dao.java",
							this.getDao().generateDaoString());
				}
	
			    	if (this.getDaoImpl() != null) {
			    		writeToFile(srcfolderPath+"daoimpl/"+StringUtil.capFirst(this.getDomain().getStandardName())+"DaoImpl.java", this.getDaoImpl().generateDaoImplString());
				}
	
				if (this.getService() != null) {
					writeToFile(srcfolderPath + "service/" + StringUtil.capFirst(this.getDomain().getStandardName())
							+ "Service.java", this.getService().generateServiceString());
				}
	
				if (this.getServiceImpl() != null) {
					writeToFile(srcfolderPath + "serviceimpl/" + StringUtil.capFirst(this.getDomain().getStandardName())
							+ "ServiceImpl.java", this.getServiceImpl().generateServiceImplString());
				}
	
				if (this.facade != null) {
					writeToFile(srcfolderPath + "facade/" + StringUtil.capFirst(this.domain.getCapFirstDomainName())
							+ "Facade.java", this.facade.generateFacadeString());
				}
	
				for (JsonPagingGridJspTemplate jsontp : this.jsonjsptemplates) {
					EasyUIPageTemplate etp = (EasyUIPageTemplate)jsontp;
					etp.setTitle(this.title);
					etp.setSubTitle(this.subTitle);
					etp.setFooter(this.footer);
					writeToFile(folderPath + "WebContent/pages/" + this.domain.getPlural().toLowerCase() + ".html",
							etp.generateJspString());
				}
	
				for (ManyToMany mtm : this.manyToManies) {
					mtm.setTitle(this.title);
					mtm.setSubTitle(this.subTitle);
					mtm.setFooter(this.footer);
					writeToFile(srcfolderPath + "domain/" + StringUtil.capFirst(mtm.getLinkDomain().getStandardName()) + ".java",
							mtm.getLinkDomain().generateClassString());
					writeToFile(folderPath + "WebContent/pages/" + mtm.getEuTemplate().getStandardName().toLowerCase() + ".html",
							mtm.getEuTemplate().generateContentString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public DaoImpl getDaoImpl() {
		return daoimpl;
	}

	public void setDaoImpl(DaoImpl daoImpl) {
		this.daoimpl = daoImpl;
	}

	public void addController(Controller c) {
		this.controllers.add(c);
	}

	public long getServiceimplClassId() {
		return serviceimplClassId;
	}

	public void setServiceimplClassId(long serviceimplClassId) {
		this.serviceimplClassId = serviceimplClassId;
	}

	public ServiceImpl getServiceimpl() {
		return serviceimpl;
	}

	public void setServiceimpl(ServiceImpl serviceimpl) {
		this.serviceimpl = serviceimpl;
	}

	public List<Controller> getControllers() {
		return controllers;
	}

	public void setControllers(List<Controller> controllers) {
		this.controllers = controllers;
	}

	public DBDefinitionGenerator getDbDefinitionGenerator() {
		return dbDefinitionGenerator;
	}

	public void setDbDefinitionGenerator(DBDefinitionGenerator dbDefinitionGenerator) {
		this.dbDefinitionGenerator = dbDefinitionGenerator;
	}

	public List<JspTemplate> getJsptemplates() {
		return jsptemplates;
	}

	public void setJsptemplates(List<JspTemplate> jsptemplates) {
		this.jsptemplates = jsptemplates;
	}

	public void addJspTemplate(JspTemplate template) {
		this.jsptemplates.add(template);
	}

	public void addJsonJspTemplate(JsonPagingGridJspTemplate template) {
		this.jsonjsptemplates.add(template);
	}

	public String getPackageToken() {
		return packageToken;
	}

	public void setPackageToken(String packagetoken) {
		if (packagetoken != null) {
			this.packageToken = packagetoken;
			if (this.dao != null)
				this.dao.setPackageToken(packagetoken);
			if (this.daoimpl != null)
				this.daoimpl.setPackageToken(packagetoken);
			if (this.serviceimpl != null)
				this.service.setPackageToken(packagetoken);
			if (this.serviceimpl != null)
				this.serviceimpl.setPackageToken(packagetoken);
			if (this.facade != null)
				this.facade.setPackageToken(packagetoken);
		}
	}

	public void generatePrismFromDomain() throws ValidateException, Exception {
		if (this.domain != null) {
			if (this.getPackageToken() != null) {
				this.domain.setPackageToken(packageToken);
			}
			this.domain.decorateCompareTo();

			this.domain = HibernateDomainDecorater.generateDecroatedDomain(this.domain);

			this.dao = new Dao();
			this.dao.setDomain(this.domain);
			this.dao.setPackageToken(this.domain.getPackageToken());
			this.daoimpl = new DaoImpl();
			this.daoimpl.setPackageToken(this.domain.getPackageToken());
			this.daoimpl.setDomain(this.domain);
			this.daoimpl.setExtendedType(new Type("BaseDao"));
			this.daoimpl.addClassImports(this.packageToken+".dao.BaseDao");
			this.daoimpl.addClassImports("org.springframework.stereotype.Repository");
			this.daoimpl.setDao(this.dao);

			this.service = new Service();
			this.service.setDomain(this.domain);
			this.service.setPackageToken(this.domain.getPackageToken());
			this.serviceimpl = new ServiceImpl(this.domain);
			this.serviceimpl.setPackageToken(this.domain.getPackageToken());
			this.serviceimpl.getDao().addAnnotation("Autowired");
			Method daoSetter = NamedUtilMethodGenerator.generateSetter("dao",
					new Type(this.domain.getCapFirstDomainName() + "Dao"));
			this.serviceimpl.addMethod(daoSetter);
			this.serviceimpl.addClassImports("org.springframework.beans.factory.annotation.Autowired");
			this.serviceimpl.setDomain(this.domain);
			this.serviceimpl.setService(this.service);

			Verb listAll = new ListAll(this.domain);
			Verb update = new Update(this.domain);
			Verb delete = new Delete(this.domain);
			Verb add = new Add(this.domain);
			Verb softdelete = new SoftDelete(this.domain);
			Verb activate = new Activate(this.domain);
			Verb findbyid = new FindById(this.domain);
			Verb findbyname = new FindByName(this.domain);
			//Verb searchbyname = new SearchByName(this.domain);
			Verb listactive = new ListActive(this.domain);
			//Verb listAllByPage = new ListAllByPage(this.domain);
			Verb deleteAll = new DeleteAll(this.domain);
			Verb softDeleteAll = new SoftDeleteAll(this.domain);
			Verb activateAll = new ActivateAll(this.domain);
			Verb toggle = new Toggle(this.domain);
			Verb toggleOne = new ToggleOne(this.domain);
			Verb searchByFieldsByPage = new SearchByFieldsByPage(this.domain);
			Verb export = new Export(this.domain);

			//CountAllPage countAllPage = new CountAllPage(this.domain);
			CountAllRecords countAllRecords = new CountAllRecords(this.domain);
			CountSearchByFieldsRecords countSearch = new CountSearchByFieldsRecords(this.domain);
			CountActiveRecords countActiveRecords = new CountActiveRecords(this.domain);

			this.addVerb(listAll);
			this.addVerb(update);
			this.addVerb(delete);
			this.addVerb(add);
			this.addVerb(softdelete);
			this.addVerb(activate);
			this.addVerb(findbyid);
			this.addVerb(findbyname);
			//this.addVerb(searchbyname);
			this.addVerb(listactive);
			//this.addVerb(listAllByPage);
			this.addVerb(deleteAll);
			this.addVerb(softDeleteAll);
			this.addVerb(activateAll);
			this.addVerb(toggle);
			this.addVerb(toggleOne);
			this.addVerb(searchByFieldsByPage);
			this.addVerb(export);

			//this.noControllerVerbs.add(countAllPage);
			this.noControllerVerbs.add(countAllRecords);
			this.noControllerVerbs.add(countSearch);
			this.noControllerVerbs.add(countActiveRecords);
			this.action = new Action(this.verbs, this.domain);
			this.action.setPackageToken(this.packageToken);
			this.facade = new SpringMVCFacade(this.verbs, this.domain);
			this.facade.setPackageToken(this.packageToken);

			for (Verb v : this.verbs) {
				v.setDomain(domain);
				service.addMethod(v.generateServiceMethodDefinition());
				serviceimpl.addMethod(v.generateServiceImplMethod());
				dao.addMethod(v.generateDaoMethodDefinition());
				daoimpl.addMethod(v.generateDaoImplMethod());
			}

			for (NoControllerVerb nVerb : this.noControllerVerbs) {
				nVerb.setDomain(domain);
				service.addMethod(nVerb.generateServiceMethodDefinition());
				serviceimpl.addMethod(nVerb.generateServiceImplMethod());
				dao.addMethod(nVerb.generateDaoMethodDefinition());
				daoimpl.addMethod(nVerb.generateDaoImplMethod());
			}

			for (DaoOnlyVerb oVerb : this.daoOnlyVerbs) {
				oVerb.setDomain(domain);
				dao.addMethod(oVerb.generateDaoMethodDefinition());
				daoimpl.addMethod(oVerb.generateDaoImplMethod());
			}

			this.mybatisDaoXmlDecorator = new MybatisDaoXmlDecorator();
			this.mybatisDaoXmlDecorator.setDomain(this.getDomain());
			Set<Domain> resultMaps = new TreeSet<Domain>();
			resultMaps.add(this.domain);
			Set<Method> daoimplMethods = new TreeSet<Method>();
			for (Verb vb : this.verbs) {
				if (vb.generateDaoImplMethod()!=null) daoimplMethods.add(vb.generateDaoImplMethod());
			}
			for (DaoOnlyVerb dovb : this.daoOnlyVerbs) {
				if (dovb.generateDaoImplMethod()!=null) daoimplMethods.add(dovb.generateDaoImplMethod());
			}
			for (NoControllerVerb ncvb : this.noControllerVerbs) {
				if (ncvb.generateDaoImplMethod()!=null) daoimplMethods.add(ncvb.generateDaoImplMethod());
			}

			EasyUIPageTemplate easyui = new EasyUIPageTemplate();
			easyui.setDomain(this.domain);
			easyui.setStandardName(this.domain.getStandardName().toLowerCase());
			easyui.setTitle(this.title);
			easyui.setSubTitle(this.subTitle);
			easyui.setFooter(this.footer);
			this.addJsonJspTemplate(easyui);

			if (this.domain.manyToManies != null && this.domain.manyToManies.size() > 0) {
				for (ManyToMany mtm : this.domain.manyToManies) {
					String slaveName = mtm.getManyToManySalveName();
					String masterName = this.domain.getStandardName();
					if (setContainsDomain(this.projectDomains, masterName)
							&& setContainsDomain(this.projectDomains, slaveName)) {
						Domain myslave = (Domain)lookupDoaminInSet(this.projectDomains, slaveName).deepClone();
						if (!StringUtil.isBlank(mtm.getSlaveAlias())){
							myslave.setAlias(mtm.getSlaveAlias());
						}
						ManyToMany mymtm = new ManyToMany(lookupDoaminInSet(this.projectDomains, masterName),
								myslave,mtm.getMasterValue(),mtm.getValues(),mtm.getSlaveAlias(),myslave.getPackageToken());
						mymtm.setSlaveAlias(myslave.getAlias());
						EasyUIManyToManyTemplate eumpt = mymtm.getEuTemplate();
						eumpt.setResolution(this.getResolution());
						mymtm.setEuTemplate(eumpt);
						this.manyToManies.add(mymtm);
						System.out.println("JerryDebug:"+mymtm+":Master:"+masterName+":"+myslave.getAlias()+"");
					} else {
						ValidateInfo validateInfo = new ValidateInfo();
						validateInfo.addCompileError("棱柱" + this.getText() + "多对多设置有误。");
						ValidateException em = new ValidateException(validateInfo);
						throw em;
					}
				}
			}
			for (ManyToMany mtm : this.manyToManies) {
				mtm.setTitle(this.title);
				mtm.setSubTitle(this.subTitle);
				mtm.setFooter(this.footer);
				mtm.setCrossOrigin(this.crossOrigin);
				this.service.addMethod(mtm.getAssign().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(mtm.getAssign().generateServiceImplMethod());
				this.dao.addMethod(mtm.getAssign().generateDaoMethodDefinition());
				this.daoimpl.addMethod(mtm.getAssign().generateDaoImplMethod());
				this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getAssign().generateDaoImplMethod());
				this.facade.addMethod(mtm.getAssign().generateFacadeMethod());

				this.service.addMethod(mtm.getRevoke().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(mtm.getRevoke().generateServiceImplMethod());
				this.dao.addMethod(mtm.getRevoke().generateDaoMethodDefinition());
				this.daoimpl.addMethod(mtm.getRevoke().generateDaoImplMethod());
				this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getRevoke().generateDaoImplMethod());
				this.facade.addMethod(mtm.getRevoke().generateFacadeMethod());

				this.service.addMethod(mtm.getListMyActive().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(mtm.getListMyActive().generateServiceImplMethod());
				this.dao.addMethod(mtm.getListMyActive().generateDaoMethodDefinition());
				this.daoimpl.addMethod(mtm.getListMyActive().generateDaoImplMethod());
				this.mybatisDaoXmlDecorator.addResultMap(mtm.getSlave());
				this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getListMyActive().generateDaoImplMethod());
				this.facade.addMethod(mtm.getListMyActive().generateFacadeMethod());

				this.service.addMethod(mtm.getListMyAvailableActive().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(mtm.getListMyAvailableActive().generateServiceImplMethod());
				this.dao.addMethod(mtm.getListMyAvailableActive().generateDaoMethodDefinition());
				this.daoimpl.addMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
				this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
				this.facade.addMethod(mtm.getListMyAvailableActive().generateFacadeMethod());
				//mtm.slave.decorateCompareTo();
				
				Service slaveService = new Service();
				slaveService.setDomain(mtm.slave);
				slaveService.addAnnotation("Autowired");
				slaveService.setStandardName(mtm.slave.getCapFirstDomainName() + "Service");
				
				Method slaveServiceSetter = NamedUtilMethodGenerator.generateSetter(mtm.slave.getLowerFirstDomainName()+"Service",
						new Type(mtm.slave.getCapFirstDomainName() + "Service",mtm.getSlave().getPackageToken()+".service."+mtm.slave.getCapFirstDomainName() + "Service"));
				this.serviceimpl.addMethod(slaveServiceSetter);
				this.serviceimpl.addOtherService(slaveService);
			}
		}
	}

	protected Boolean setContainsDomain(Set<Domain> set, String domainName) {
		for (Domain d : set) {
			if (d.getStandardName().equals(domainName))
				return true;
		}
		return false;
	}

	protected Domain lookupDoaminInSet(Set<Domain> set, String domainName) {
		for (Domain d : set) {
			if (d.getStandardName().equals(domainName))
				return d;
		}
		return null;
	}

	public static String packagetokenToFolder(String packageToken) {
		String folder = packageToken.replace('.', '/');
		folder += "/";
		return folder;
	}

	public static String folderToPackageToken(String folder) {
		String packagetoken = folder.replace('/', '.');
		if (packagetoken.charAt(packagetoken.length() - 1) == '.')
			packagetoken = packagetoken.substring(0, packagetoken.length() - 1);
		return packagetoken;
	}

	public TestSuite getPrismTestSuite() {
		return prismTestSuite;
	}

	public void setPrismTestSuite(TestSuite prismTestSuite) {
		this.prismTestSuite = prismTestSuite;
	}

	public TestCase getDaoImplTestCase() {
		return daoImplTestCase;
	}

	public void setDaoImplTestCase(TestCase daoImplTestCase) {
		this.daoImplTestCase = daoImplTestCase;
	}

	public TestCase getServiceImplTestCase() {
		return serviceImplTestCase;
	}

	public void setServiceImplTestCase(TestCase serviceImplTestCase) {
		this.serviceImplTestCase = serviceImplTestCase;
	}

	public ValidateInfo validate() {
		List<ValidateInfo> vl = new ArrayList<ValidateInfo>();
		ValidateInfo info = this.getDomain().validate();
		vl.add(info);
		for (Controller c : this.controllers) {
			ValidateInfo info2 = c.validate();
			vl.add(info2);
		}
		return ValidateInfo.mergeValidateInfo(vl);
	}

	public void expandPackageToken() {
		if (this.packageToken != null && !"".equals(this.packageToken)) {
			if (this.domain != null)
				this.domain.setPackageToken(this.packageToken);
			if (this.dao != null)
				this.dao.setPackageToken(this.packageToken);
			if (this.daoimpl != null)
				this.daoimpl.setPackageToken(this.packageToken);
			if (this.service != null)
				this.service.setPackageToken(this.packageToken);
			if (this.serviceimpl != null)
				this.serviceimpl.setPackageToken(this.packageToken);

			for (Class c : this.classes) {
				c.setPackageToken(this.packageToken);
			}
			for (Controller ctl : this.controllers) {
				ctl.setPackageToken(this.packageToken);
			}

			if (this.prismTestSuite != null)
				this.prismTestSuite.setPackageToken(this.packageToken);
			if (this.daoImplTestCase != null)
				this.daoImplTestCase.setPackageToken(this.packageToken);
			if (this.serviceImplTestCase != null)
				this.serviceImplTestCase.setPackageToken(this.packageToken);
		}
	}

	@Override
	public int compareTo(Object o) {
		String myName = this.getStandardName();
		String otherName = ((Prism) o).getStandardName();
		return myName.compareTo(otherName);
	}

	@Override
	public boolean equals(Object o) {
		return (this.compareTo((Prism) o) == 0);
	}

	public List<Verb> getVerbs() {
		return verbs;
	}

	public void setVerbs(List<Verb> verbs) {
		this.verbs = verbs;
	}

	public void addVerb(Verb verb) {
		this.verbs.add(verb);
	}

	public List<JsonPagingGridJspTemplate> getJsonjsptemplates() {
		return jsonjsptemplates;
	}

	public void setJsonjsptemplates(List<JsonPagingGridJspTemplate> jsonjsptemplates) {
		this.jsonjsptemplates = jsonjsptemplates;
	}

	public void writeToFile(String filePath, String content) throws Exception {
		File f = new File(filePath);
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		f.createNewFile();
		try (Writer fw = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath()), "UTF-8"))) {
			fw.write(content, 0, content.length());
		}
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getText() {
		if (this.label != null && !this.label.equals(""))
			return this.label;
		else
			return this.standardName;
	}

	public Set<Domain> getProjectDomains() {
		return projectDomains;
	}

	public void setProjectDomains(Set<Domain> projectDomains) {
		this.projectDomains = projectDomains;
	}
	
	public String toString(){
		return JSONObject.fromObject(this).toString();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getCrossOrigin() {
		return crossOrigin;
	}

	public void setCrossOrigin(String crossOrigin) {
		this.crossOrigin = crossOrigin;
	}

	public Set<Pair> getManyToManySlaveNames() {
		return manyToManySlaveNames;
	}

	public void setManyToManySlaveNames(Set<Pair> manyToManySlaveNames) {
		this.manyToManySlaveNames = manyToManySlaveNames;
	}

	public Set<ManyToMany> getManyToManies() {
		return manyToManies;
	}

	public void setManyToManies(Set<ManyToMany> manyToManies) {
		this.manyToManies = manyToManies;
	}
}
