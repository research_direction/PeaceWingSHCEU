package org.peacewing.domain;

import java.io.Serializable;
import java.util.Comparator;

public class FieldComparator implements Comparator<Field>,Serializable{

	@Override
	public int compare(Field o1, Field o2) {
		return o1.getFieldName().compareTo(o2.getFieldName());
	}

}
