package org.peacewing.domain;

import java.io.Serializable;

import org.peacewing.complexverb.Assign;
import org.peacewing.complexverb.ListMyActive;
import org.peacewing.complexverb.ListMyAvailableActive;
import org.peacewing.complexverb.Revoke;
import org.peacewing.easyui.EasyUIManyToManyTemplate;
import org.peacewing.generator.MysqlTwoDomainsDBDefinitionGenerator;
import org.peacewing.utils.StringUtil;

public class ManyToMany implements Comparable<ManyToMany>, Serializable{
	private static final long serialVersionUID = -8844037024490738355L;
	protected Domain master;
	protected Domain slave;
	protected Assign assign;
	protected Revoke revoke;
	protected ListMyActive listMyActive;
	protected ListMyAvailableActive listMyAvailableActive;
	protected EasyUIManyToManyTemplate euTemplate;
	protected String standardName;
	protected String label;
	protected String values;
	protected String manyToManyMasterName;
	protected String manyToManySalveName;
	protected String masterValue;
	protected LinkDomain linkDomain;
	protected String title = "";
	protected String subTitle = "";
	protected String footer = "";
	protected String crossOrigin = "";
	protected String slaveAlias = "";
	protected String resolution = "low";
	protected String slaveAliasLabel;
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
		if (this.euTemplate!=null) this.euTemplate.setResolution(resolution);
	}

	public ManyToMany(){
		super();
	}
	
	public ManyToMany(String manyToManyMasterName,String manyToManySalveName){
		super();
		this.manyToManyMasterName = manyToManyMasterName;
		this.manyToManySalveName = manyToManySalveName;
	}
	
	public ManyToMany(Domain master,Domain slave,String masterValue, String values,String slaveAlias,String packageToken){
		this.master = master;
		this.slave = slave;
		this.slave.setAlias(slaveAlias);
		this.linkDomain = new LinkDomain(packageToken,master,slave);
		this.linkDomain.setDbPrefix(this.master.getDbPrefix());
		this.standardName = this.master.getStandardName()+this.slave.getCapFirstAliasOrName();
		this.assign = new Assign(master,slave);
		this.revoke = new Revoke(master,slave);
		this.masterValue = masterValue;
		this.values = values;
		this.slaveAlias = slaveAlias;
		this.listMyActive = new ListMyActive(master,slave);
		this.listMyAvailableActive = new ListMyAvailableActive(master,slave);
		this.assign.setLinkDomain(linkDomain);
		this.revoke.setLinkDomain(linkDomain);
		this.listMyActive.setLinkDomain(linkDomain);
		this.listMyAvailableActive.setLinkDomain(linkDomain);
		this.euTemplate = new EasyUIManyToManyTemplate(master,slave);
		this.euTemplate.setResolution(this.resolution);
	}
	
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}
	public Assign getAssign() {
		return assign;
	}
	public void setAssign(Assign assign) {
		this.assign = assign;
	}
	public Revoke getRevoke() {
		return revoke;
	}
	public void setRevoke(Revoke revoke) {
		this.revoke = revoke;
	}
	public ListMyActive getListMyActive() {
		return listMyActive;
	}
	public void setListMyActive(ListMyActive listMyActive) {
		this.listMyActive = listMyActive;
	}
	public ListMyAvailableActive getListMyAvailableActive() {
		return listMyAvailableActive;
	}
	public void setListMyAvailableActive(ListMyAvailableActive listMyAvailableActive) {
		this.listMyAvailableActive = listMyAvailableActive;
	}
	public EasyUIManyToManyTemplate getEuTemplate() {
		return euTemplate;
	}
	public void setEuTemplate(EasyUIManyToManyTemplate euTemplate) {
		this.euTemplate = euTemplate;
	}
	public Domain getMaster() {
		return master;
	}
	public String getStandardName(){
		if (this.master !=null && this.slave !=null){
			if (StringUtil.isBlank(this.slave.getAlias())){
				return "Link"+this.master.getStandardName()+this.slave.getStandardName();
			}else {
				return "Link"+this.master.getStandardName()+this.slave.getAlias();
			}
		} else {
			return this.standardName;
		}
	}
	@Override
	public int compareTo(ManyToMany o) {
		if (!StringUtil.isBlank(this.standardName)&&!StringUtil.isBlank(o.getStandardName())) {
			return this.getStandardName().compareTo(o.getStandardName());
		} else {
			if (this.master!=null && this.slave != null && o.getMaster()!=null && o.getSlave()!=null) {
				int result = this.master.compareTo(o.getMaster());
				if (result == 0) {
					result = this.slave.compareTo(o.getSlave());
				}else {
					result = this.slaveAlias.compareToIgnoreCase(o.getSlaveAlias());
				}
				return result;
			}else {
				int result = this.manyToManyMasterName.compareTo(o.getManyToManyMasterName());
				if (result == 0) {
					String slaveAliasOrName = StringUtil.isBlank(this.slaveAlias)?this.manyToManySalveName:this.slaveAlias;
					String oAliasOrName = StringUtil.isBlank(o.getSlaveAlias())?o.getManyToManySalveName():o.getSlaveAlias();
					result = slaveAliasOrName.compareTo(oAliasOrName);
				}
				return result;
			}
		}
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getText(){
		if (StringUtil.isBlank(this.master.getLabel())||StringUtil.isEnglishAndDigitalAndEmpty(this.master.getLabel())){
			if (StringUtil.isBlank(this.slave.getAlias())) {
				return "Link "+this.master.getStandardName()+" "+this.slave.getStandardName();
			} else {
				return "Link "+this.master.getStandardName()+" "+this.slave.getAlias();
			}
		}
		else return "链接"+this.master.getText()+this.slave.getText();
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getManyToManySalveName() {
		return manyToManySalveName;
	}

	public void setManyToManySalveName(String manyToManySalveName) {
		this.manyToManySalveName = manyToManySalveName;
	}

	public String getMasterValue() {
		return masterValue;
	}

	public void setMasterValue(String masterValue) {
		this.masterValue = masterValue;
	}

	public LinkDomain getLinkDomain() {
		return linkDomain;
	}

	public void setLinkDomain(LinkDomain linkDomain) {
		this.linkDomain = linkDomain;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		this.euTemplate.setTitle(title);
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
		this.euTemplate.setSubTitle(subTitle);
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
		this.euTemplate.setFooter(footer);
	}

	public String getCrossOrigin() {
		return crossOrigin;
	}

	public void setCrossOrigin(String crossOrigin) {
		this.crossOrigin = crossOrigin;
	}

	public String getSlaveAlias() {
		if (!StringUtil.isBlank(this.slaveAlias)) {
			return this.slaveAlias;
		} else {
			return this.manyToManySalveName;
		}
	}

	public void setSlaveAlias(String slaveAlias) {
		this.slaveAlias = slaveAlias;
	}

	public String getManyToManyMasterName() {
		return manyToManyMasterName;
	}

	public void setManyToManyMasterName(String manyToManyMasterName) {
		this.manyToManyMasterName = manyToManyMasterName;
	}

	public String getSlaveAliasLabel() {
		return slaveAliasLabel;
	}

	public void setSlaveAliasLabel(String slaveAliasLabel) {
		this.slaveAliasLabel = slaveAliasLabel;
	}

	public String getSlaveAliasLabelOrText() {
		if (!StringUtil.isBlank(this.slaveAliasLabel)) return this.getSlaveAliasLabel();
		else return this.slave.getText();
	}
	
	public MysqlTwoDomainsDBDefinitionGenerator toTwoDBGenerator(){
		MysqlTwoDomainsDBDefinitionGenerator mtg = new MysqlTwoDomainsDBDefinitionGenerator(this.master,this.slave);
		return mtg;
	}
}
