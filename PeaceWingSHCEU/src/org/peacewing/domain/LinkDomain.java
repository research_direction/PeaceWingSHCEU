package org.peacewing.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Writeable;
import org.peacewing.utils.PluralUtil;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class LinkDomain extends Class implements Comparable<LinkDomain>,Serializable{
	private static final long serialVersionUID = -2928263054208991443L;
	protected Domain master;
	protected Domain slave;
	protected String label;
	protected String dbPrefix;
	protected String plural;
	protected String packageToken;
	
	public int compareTo(LinkDomain o) {
		int result = this.getStandardName().compareTo(o.getStandardName());
		if (result == 0) {
			result = this.master.getDomainId().compareTo(o.getMaster().getDomainId());
		}else if (result == 0) {
			result = this.slave.getDomainId().compareTo(o.getSlave().getDomainId());
		}
		return result;
	}
	
	public String getPlural() {
		if (this.plural == null || "".equals(this.plural)){
			return this.master.getCapFirstDomainName()+PluralUtil.lookupPlural(this.slave.getCapFirstAliasOrName());
		} else {
			return StringUtil.capFirst(plural);
		}
	}
	
	public void setPlural(String plural) {
		this.plural = StringUtil.capFirst(plural);
	}
	
	public Type getType(){
		Type retType = new Type(this.getStandardName(),this.getPackageToken());
		return retType;
	}
	
	public String generateTypeVarString(){
		return StringUtil.capFirst(this.getStandardName()) + " " + StringUtil.lowerFirst(this.getStandardName());
	}

	public String getDbPrefix() {
		if (dbPrefix == null && "".equals(dbPrefix)) return "";
		else return dbPrefix;
	}

	public void setDbPrefix(String dbPrefix) {
		this.dbPrefix = dbPrefix;
	}
	
	public String getLowerFirstLinkDomainName(){
		return StringUtil.lowerFirst(this.getStandardName());
	}
	
	public String getLowerFirstTwoChar(){
		return this.master.getStandardName().substring(0,1).toLowerCase()+this.slave.getAliasOrName().substring(0,1).toLowerCase();
	}
	
	public String getCapFirstLinkDomainName(){
		return StringUtil.capFirst(this.getStandardName());
	}
	
	public String getFullName(){
		return this.getPackageToken()+".domain."+this.getCapFirstLinkDomainName();
	}
	
	public String getCapFirstPlural(){
		return StringUtil.capFirst(this.getPlural());
	}
	
	public String getLowerFirstPlural(){
		return StringUtil.lowerFirst(this.getPlural());
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.getStandardName();
	}


	public Domain getMaster() {
		return master;
	}

	public void setMaster(Domain master) {
		this.master = master;
	}

	public Domain getSlave() {
		return slave;
	}

	public void setSlave(Domain slave) {
		this.slave = slave;
	}

	public boolean equals(Object o){
		return this.standardName.equals(((LinkDomain)o).getStandardName());
	}

	public String getPackageToken() {
		return packageToken;
	}

	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}	
	
	public LinkDomain(String packageToken,Domain master,Domain slave){
		super();
		this.packageToken = packageToken;
		this.master = master;
		this.slave = slave;
		this.standardName = master.getCapFirstDomainName()+slave.getCapFirstAliasOrName();
	}
	
	public String toTableName(){
		return this.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(master.getCapFirstDomainName())+"_"+StringUtil.changeDomainFieldtoTableColum(slave.getCapFirstAliasOrName());
	}
	
	@Override
	public String generateClassString(){
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"package "+this.packageToken+".domain;"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"import java.io.Serializable;"));
		sList.add(new Statement(4000L,0,""));
		sList.add(new Statement(5000L,0,"import javax.persistence.Entity;"));
		sList.add(new Statement(6000L,0,"import javax.persistence.Id;"));
		sList.add(new Statement(7000L,0,"import javax.persistence.JoinColumn;"));
		sList.add(new Statement(8000L,0,"import javax.persistence.ManyToOne;"));
		sList.add(new Statement(9000L,0,"import javax.persistence.Table;"));
		sList.add(new Statement(10000L,0,""));
		sList.add(new Statement(11000L,0,"@Entity"));
		sList.add(new Statement(12000L,0,"@Table(name = \""+this.toTableName()+"\")"));
		sList.add(new Statement(13000L,0,"public class "+this.standardName+" implements Comparable<"+this.standardName+">,Serializable {"));
		sList.add(new Statement(15000L,1,"@Id"));
		sList.add(new Statement(16000L,1,"@ManyToOne"));
		sList.add(new Statement(17000L,1,"@JoinColumn(name =\""+StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+"Id")+"\", nullable = false)"));
		sList.add(new Statement(18000L,1,"private "+this.master.getCapFirstDomainName()+" "+this.master.getLowerFirstDomainName()+";"));
		sList.add(new Statement(19000L,1,""));
		sList.add(new Statement(20000L,1,"@Id"));
		sList.add(new Statement(21000L,1,"@ManyToOne"));
		sList.add(new Statement(22000L,1,"@JoinColumn(name =\""+StringUtil.changeDomainFieldtoTableColum(slave.getAliasOrName()+"Id")+"\", nullable = false)"));
		sList.add(new Statement(23000L,1,"private "+this.slave.getCapFirstDomainName()+" "+this.slave.getLowerFirstAliasOrName()+";"));
		sList.add(new Statement(24000L,0,""));
		sList.add(new Statement(25000L,1,"public "+this.master.getCapFirstDomainName()+" get"+this.master.getCapFirstDomainName()+"() {"));
		sList.add(new Statement(26000L,2,"return "+this.master.getLowerFirstDomainName()+";"));
		sList.add(new Statement(27000L,1,"}"));
		sList.add(new Statement(28000L,0,""));
		sList.add(new Statement(29000L,1,"public void set"+this.master.getCapFirstDomainName()+"("+this.master.getCapFirstDomainName()+" "+this.master.getLowerFirstDomainName()+") {"));
		sList.add(new Statement(30000L,2,"this."+this.master.getLowerFirstDomainName()+" = "+this.master.getLowerFirstDomainName()+";"));
		sList.add(new Statement(31000L,1,"}"));
		sList.add(new Statement(32000L,0,""));
		sList.add(new Statement(33000L,1,"public "+this.slave.getCapFirstDomainName()+" get"+this.slave.getCapFirstAliasOrName()+"() {"));
		sList.add(new Statement(34000L,2,"return "+this.slave.getLowerFirstAliasOrName()+";"));
		sList.add(new Statement(35000L,1,"}"));
		sList.add(new Statement(36000L,0,""));
		sList.add(new Statement(37000L,1,"public void set"+this.slave.getCapFirstAliasOrName()+"("+this.slave.getCapFirstDomainName()+" "+this.slave.getLowerFirstAliasOrName()+") {"));
		sList.add(new Statement(38000L,2,"this."+this.slave.getLowerFirstAliasOrName()+" = "+this.slave.getLowerFirstAliasOrName()+";"));
		sList.add(new Statement(39000L,1,"}"));
		sList.add(new Statement(40000L,0,""));
		sList.add(new Statement(41000L,1,"@Override"));
		sList.add(new Statement(42000L,1,"public int compareTo("+this.standardName+" arg0) {"));
		sList.add(new Statement(43000L,2,"int ret = this."+this.master.getLowerFirstDomainName()+".getId().compareTo(arg0.get"+this.master.getCapFirstDomainName()+"().getId());"));
		sList.add(new Statement(44000L,2,"if (ret == 0) {"));
		sList.add(new Statement(45000L,3,"return this."+this.slave.getLowerFirstAliasOrName()+".getId().compareTo(arg0.get"+this.slave.getCapFirstAliasOrName()+"().getId());"));
		sList.add(new Statement(46000L,2,"}"));
		sList.add(new Statement(47000L,2,"else return ret;"));
		sList.add(new Statement(48000L,1,"}"));
		sList.add(new Statement(49000L,0,"}"));
		return WriteableUtil.merge(sList).getContent();
	}
}
