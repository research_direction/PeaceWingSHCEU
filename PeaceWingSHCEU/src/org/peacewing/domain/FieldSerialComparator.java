package org.peacewing.domain;

import java.io.Serializable;
import java.util.Comparator;

public class FieldSerialComparator implements Comparator<Field>,Serializable{

	@Override
	public int compare(Field o1, Field o2) {
		return ((Long)o1.getSerial()).compareTo(o2.getSerial());
	}

}
