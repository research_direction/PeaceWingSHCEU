package org.peacewing.limitedverb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Field;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.utils.DomainTokenUtil;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.MybatisSqlReflector;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.TableStringUtil;
import org.peacewing.utils.WriteableUtil;

public class CountSearchByFieldsRecords extends NoControllerVerb {

	@Override
	public Method generateDaoImplMethod() throws Exception {		
		Method method = new Method();
		method.setStandardName("countSearch"+this.domain.getCapFirstPlural()+"ByFieldsRecords");
		method.setReturnType(new Type("Integer"));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.hibernate.Criteria");
		method.addAdditionalImport("org.hibernate.criterion.Restrictions");
		method.addAdditionalImport("org.hibernate.criterion.MatchMode");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,this.domain.getLowerFirstDomainName(),this.domain.getType(),this.domain.getPackageToken()));
		method.setThrowException(true);
		method.addMetaData("Override");
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(3000L,2, "Session s = this.getHibernateTemplate().getSessionFactory().getCurrentSession();"));
		list.add(new Statement(4000L,2, "Criteria criteria = s.createCriteria("+this.domain.getStandardName()+".class);"));
		
		long serial = 5000L;
		Set<Field> fields = this.domain.getFieldsWithoutId();
		for (Field f:fields){
			if (f.getFieldType().equalsIgnoreCase("string")){
				list.add(new Statement(serial,2,"if (null != "+this.domain.getLowerFirstDomainName()+"."+f.getGetterCall()+" && !\"\".equals("+this.domain.getLowerFirstDomainName()+"."+f.getGetterCall()+")){"));
				list.add(new Statement(serial+100,3,"criteria.add(Restrictions.like(\""+f.getLowerFirstFieldName()+"\","+this.domain.getLowerFirstDomainName()+"."+f.getGetterCall()+",MatchMode.ANYWHERE));"));
				list.add(new Statement(serial+200,2,"}"));
			}else{
				list.add(new Statement(serial,2,"if (null != "+this.domain.getLowerFirstDomainName()+"."+f.getGetterCall()+"){"));
				list.add(new Statement(serial+100,3,"criteria.add(Restrictions.eq(\""+f.getLowerFirstFieldName()+"\","+this.domain.getLowerFirstDomainName()+"."+f.getGetterCall()+"));"));
				list.add(new Statement(serial+200,2,"}"));
			}
			serial += 1000L;
		}
        list.add(new Statement(serial,2, "return criteria.list().size();"));
		
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception{
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception{
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("countSearch"+this.domain.getCapFirstPlural()+"ByFieldsRecords");
		method.setReturnType(new Type("Integer"));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,this.domain.getLowerFirstDomainName(),this.domain.getType(),this.domain.getPackageToken()));		
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception{
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("countSearch"+this.domain.getCapFirstPlural()+"ByFieldsRecords");
		method.setReturnType(new Type("Integer"));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,this.domain.getLowerFirstDomainName(),this.domain.getType()));
		method.setThrowException(true);
		
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception{
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("countSearch"+this.domain.getCapFirstPlural()+"ByFieldsRecords");
		method.setReturnType(new Type("Integer"));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,this.domain.getLowerFirstDomainName(),this.domain.getType()));
		method.setThrowException(true);
		method.addMetaData("Override");
		
		Method daomethod = this.generateDaoMethodDefinition();		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,2,"return "+daomethod.generateStandardServiceImplCallString(InterVarUtil.DB.dao.getVarName())+";"));
		method.setMethodStatementList(WriteableUtil.merge(list));

		return method;
	}

	@Override
	public String generateServiceImplMethodString()throws Exception{
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception{
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public CountSearchByFieldsRecords(Domain domain){
		super();
		this.domain = domain;
		this.verbName = "countSearch"+this.domain.getCapFirstPlural()+"ByFieldsRecords";
	}
	
	public CountSearchByFieldsRecords(){
		super();
	}

}
