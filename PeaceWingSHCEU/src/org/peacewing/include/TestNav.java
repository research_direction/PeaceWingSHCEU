package org.peacewing.include;

import org.peacewing.domain.Include;
import org.peacewing.domain.StatementList;

public class TestNav extends Include{
	public TestNav(){
		super();
		this.fileName = "testnav.jsp";
		this.packageToken = "";
	}

	@Override
	public String generateIncludeString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<!-- Common Navigation Panel for our site -->\n");
		sb.append("<li id=\"submenu\">\n");
		sb.append("\t<h2>Select an option</h2>\n");
		sb.append("\t<ul>\n");
		sb.append("\t\t<li><a href=\"../jsp/index.jsp\">Admin Home</a></li>\n");   
		sb.append("\t</ul>\n");
		sb.append("</li>\n");
		return sb.toString();
	}

	@Override
	public StatementList getStatementList(long serial, int indent) {
		// TODO Auto-generated method stub
		return null;
	}
}
