package org.javaforever.poitranslator.core;

import java.util.ArrayList;
import java.util.List;

public class ActionGroup {
	protected Boolean success = true;
	protected HeadRow headRow = new HeadRow();
	protected List<Action> actions = new ArrayList<Action>();
	protected String sheetName = "LoginTest";
	
	public void addAction(Action action){
		this.actions.add(action);
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public HeadRow getHeadRow() {
		return headRow;
	}

	public void setHeadRow(HeadRow headRow) {
		this.headRow = headRow;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	
	public boolean success(){
		boolean success = true;
		for (Action ac:this.getActions()){
			if (!ac.success())
				success = false;
		}
		return success;
	}
}
